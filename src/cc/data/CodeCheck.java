/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.data;

import static djf.settings.AppStartupConstants.PATH_WORK;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author aespi
 */
public class CodeCheck {
    private StringProperty codeCheckName;
    private String codeCheckAddress;
    
    public CodeCheck(String codeCheckName) {
        this.codeCheckName = new SimpleStringProperty(codeCheckName);
        codeCheckAddress = PATH_WORK + codeCheckName.toString();
        
    }
    
    public String getCodeCheckName() {
        return codeCheckName.get();
    }

    public void setCodeCheckName(String name) {
        codeCheckName.setValue(name);        
    }

    public void setCodeCheckAddress(String address){
        this.codeCheckAddress = address;
    }

    String getCodeCheckAddress() {
        return codeCheckAddress;
    }
    
    
}
