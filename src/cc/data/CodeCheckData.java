/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.data;

import cc.CodeCheckApp;
import djf.components.AppDataComponent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author aespi
 */
public class CodeCheckData implements AppDataComponent {

    // We'll need access to the app to notify the GUI when data changes.
    CodeCheckApp app;

    // We'll use this data structure to directly store the
    // code check object which we'll use to get the address of the 
    // directories that will be manipulated.
    ObservableList<String> extractSubmissionsList;
    private CodeCheck codeCheck;
    
    /**
     * This constructor will setup the required data structure for use.
     * @param initApp The application this data manager belongs to.
     */
    public CodeCheckData(CodeCheckApp initApp) {
        // Keep this to communicate with the rest of the application.
        app = initApp;        
        extractSubmissionsList = FXCollections.observableArrayList();
        codeCheck = new CodeCheck("");
    }
    
    public void setCodeCheckName(String name){
        codeCheck.setCodeCheckName(name);
    }
    
    @Override
    public void resetData() {
        
    }

    public String getCodeCheckName() {
        return codeCheck.getCodeCheckName();
    }
    
    private void fillWithDummyData(){
        extractSubmissionsList.add("Students AthroughG.zip");
        extractSubmissionsList.add("Students HthroughO.zip");
        extractSubmissionsList.add("Students PthroughZ.zip");
    }
    
    public String getExtractSubmissions(){
        return "";
    }
    
    public void setCodeCheckAddress(String address){
        codeCheck.setCodeCheckAddress(address);
    }

    public String getCodeCheckAddress() {
        return codeCheck.getCodeCheckAddress();
    }
}
