/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.workspace;

import cc.CodeCheckApp;
import static cc.CodeCheckProp.PROJECTS_PATH;
import static cc.CodeCheckProp.SUBMISSIONS_PATH;
import static cc.CodeCheckProp.UNZIP_SUBMISSIONS_BUTTON_MESSAGE;
import static cc.CodeCheckProp.UNZIP_SUBMISSIONS_DIRECTIONS_MESSAGE;
import static cc.CodeCheckProp.UNZIP_SUBMISSIONS_LABEL_MESSAGE;
import static cc.CodeCheckProp.UNZIP_SUBMISSIONS_LISTVIEW_TITLE;
import static cc.CodeCheckProp.UNZIP_SUBMISSIONS_PROGRESS_MESSAGE;
import static cc.CodeCheckProp.WORKSPACE_REFRESH_BUTTON_MESSAGE;
import static cc.CodeCheckProp.WORKSPACE_REMOVE_BUTTON_MESSAGE;
import static cc.CodeCheckProp.WORKSPACE_VIEW_BUTTON_MESSAGE;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_DEFAULT_BUTTONS;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_LISTVIEW;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_LISTVIEW_LABEL;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_MAIN_LABEL;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_PROGRESS_BAR;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_SECONDARY_LABEL;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_VIEW;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import properties_manager.PropertiesManager;

/**
 *
 * @author aespi
 */
public class UnzipSubmissionView extends BorderPane {  

    CodeCheckApp app;
    CodeCheckController controller;

    private Label unzipSubLabel;
    private Label unzipDirectionsLabel;
    private Label progressLabel;
    private Label listViewLabel;
    private ListView unzipSubListView;
    private TextArea unzipSubTextArea;
    private Button unzipButton;
    private Button removeSubButton;
    private Button refreshSubButton;
    private Button viewSubButton;
    private ProgressBar unzipProgressBar;
    private ProgressIndicator unzipProgressIndicator;
    private GridPane mainViewGridPane;
    private HBox buttonsHBox;
    private HBox progressHBox;

    private String unzipSubmissionsPath;
    private String renameSubmissionsPath;

    public UnzipSubmissionView(CodeCheckApp initApp) {
        app = initApp;
        initLayout();
        initControllers();
        initStyle();

        unzipSubmissionsPath = PropertiesManager.getPropertiesManager().getProperty(PROJECTS_PATH);
        renameSubmissionsPath = PropertiesManager.getPropertiesManager().getProperty(SUBMISSIONS_PATH);
    }

    private void initLayout() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        unzipSubLabel = new Label(props.getProperty(UNZIP_SUBMISSIONS_LABEL_MESSAGE));
        unzipDirectionsLabel = new Label(props.getProperty(UNZIP_SUBMISSIONS_DIRECTIONS_MESSAGE));
        progressLabel = new Label(props.getProperty(UNZIP_SUBMISSIONS_PROGRESS_MESSAGE));
        listViewLabel = new Label(props.getProperty(UNZIP_SUBMISSIONS_LISTVIEW_TITLE));
        unzipButton = new Button(props.getProperty(UNZIP_SUBMISSIONS_BUTTON_MESSAGE));
        removeSubButton = new Button(props.getProperty(WORKSPACE_REMOVE_BUTTON_MESSAGE));
        refreshSubButton = new Button(props.getProperty(WORKSPACE_REFRESH_BUTTON_MESSAGE));
        viewSubButton = new Button(props.getProperty(WORKSPACE_VIEW_BUTTON_MESSAGE));
        viewSubButton.setDisable(true);
        removeSubButton.setDisable(true);
        unzipButton.setDisable(true);
        unzipSubListView = new ListView();
        unzipSubListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        unzipProgressBar = new ProgressBar();
        unzipProgressBar.setProgress(0.00F);

        unzipProgressIndicator = new ProgressIndicator();
        unzipProgressIndicator.setProgress(0.00F);
        unzipSubTextArea = new TextArea();
        unzipSubTextArea.setEditable(false);
        unzipSubTextArea.setWrapText(true);

        buttonsHBox = new HBox(5);
        buttonsHBox.getChildren().add(removeSubButton);
        buttonsHBox.getChildren().add(refreshSubButton);
        buttonsHBox.getChildren().add(viewSubButton);

        progressHBox = new HBox(5);
        progressHBox.getChildren().add(progressLabel);
        progressHBox.getChildren().add(unzipProgressBar);
        progressHBox.getChildren().add(unzipProgressIndicator);

        GridPane.setHgrow(this, Priority.ALWAYS);
        GridPane.setVgrow(this, Priority.ALWAYS);
        mainViewGridPane = new GridPane();
        mainViewGridPane.setVgap(10);
        mainViewGridPane.setHgap(50);
        //mainViewGridPane.setGridLinesVisible(true);
        mainViewGridPane.prefWidthProperty().bind(app.getGUI().getWindow().widthProperty().divide(1));
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(50);
        mainViewGridPane.getColumnConstraints().add(column1);

        mainViewGridPane.add(unzipSubLabel, 0, 0);
        mainViewGridPane.add(unzipDirectionsLabel, 0, 1);
        mainViewGridPane.add(listViewLabel, 0, 2);
        mainViewGridPane.add(unzipSubListView, 0, 3);
        mainViewGridPane.add(buttonsHBox, 0, 4);
        mainViewGridPane.add(progressHBox, 1, 0);
        mainViewGridPane.add(unzipButton, 1, 1);
        mainViewGridPane.add(unzipSubTextArea, 1, 3);

        this.setCenter(mainViewGridPane);
    }

    private void initControllers() {
        // *** POSSIBLE SOURCE OF FUTURE BUG
        // DOES HAVING MULTIPLE INSTANCES OF THE CONTROLLER CAUSE ISSUES? YES

        controller = new CodeCheckController(app);
        removeSubButton.setOnAction(e -> controller.handleZipRemove(unzipSubListView, renameSubmissionsPath));
        refreshSubButton.setOnAction(e -> controller.handleRefresh(unzipSubListView, renameSubmissionsPath));
        viewSubButton.setOnAction(e -> controller.handleZipView(unzipSubListView, renameSubmissionsPath));

        unzipButton.setOnAction(e -> controller.handleExtractStudentSubmission(unzipSubListView,
                renameSubmissionsPath, unzipSubmissionsPath));
        unzipSubListView.setOnMouseClicked(e ->{
            Object itemSelected = unzipSubListView.getSelectionModel().getSelectedItem();
            if(itemSelected != null){
                viewSubButton.setDisable(false);
                removeSubButton.setDisable(false);
                unzipButton.setDisable(false);
                
            } else{
                
                System.out.println("Something was NOT selected");
            }
        });
        this.setOnMouseClicked(e -> {
            unzipSubListView.getSelectionModel().clearSelection();
            viewSubButton.setDisable(true);
            removeSubButton.setDisable(true);
            unzipButton.setDisable(true);
        });
        unzipSubTextArea.setOnMouseClicked(e -> {
            unzipSubListView.getSelectionModel().clearSelection();
            viewSubButton.setDisable(true);
            removeSubButton.setDisable(true);
            unzipButton.setDisable(true);
        });
    }

    private void initStyle() {
        this.getStyleClass().add(CLASS_WORKSPACE_VIEW);
        unzipSubLabel.getStyleClass().add(CLASS_WORKSPACE_MAIN_LABEL);
        unzipDirectionsLabel.getStyleClass().add(CLASS_WORKSPACE_SECONDARY_LABEL);
        listViewLabel.getStyleClass().add(CLASS_WORKSPACE_LISTVIEW_LABEL);
        unzipSubListView.getStyleClass().add(CLASS_WORKSPACE_LISTVIEW);
        progressLabel.getStyleClass().add(CLASS_WORKSPACE_MAIN_LABEL);
        removeSubButton.getStyleClass().add(CLASS_WORKSPACE_DEFAULT_BUTTONS);
        refreshSubButton.getStyleClass().add(CLASS_WORKSPACE_DEFAULT_BUTTONS);
        viewSubButton.getStyleClass().add(CLASS_WORKSPACE_DEFAULT_BUTTONS);
        unzipButton.getStyleClass().add(CLASS_WORKSPACE_DEFAULT_BUTTONS);
        unzipProgressBar.getStyleClass().add(CLASS_WORKSPACE_PROGRESS_BAR);
        mainViewGridPane.setPadding(new Insets(25, 0, 0, 20));
        removeSubButton.setPadding(new Insets(5, 35, 5, 35));
        refreshSubButton.setPadding(new Insets(5, 35, 5, 35));
        viewSubButton.setPadding(new Insets(5, 35, 5, 35));
        unzipButton.setPadding(new Insets(5, 35, 5, 35));
    }
    
    public TextArea getUnzipSubTextArea() {
        return unzipSubTextArea;
    }

    public ProgressBar getUnzipProgressBar() {
        return unzipProgressBar;
    }

    public ProgressIndicator getUnzipProgressIndicator() {
        return unzipProgressIndicator;
    }
    
    public ListView getUnzipSubListView() {
        return unzipSubListView;
    }
}
