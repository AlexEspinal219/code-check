/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.workspace;

import cc.CodeCheckApp;
import static cc.CodeCheckProp.CODE_CHECK_BUTTON_MESSAGE;
import static cc.CodeCheckProp.CODE_CHECK_DIRECTIONS_MESSAGE;
import static cc.CodeCheckProp.CODE_CHECK_LABEL_MESSAGE;
import static cc.CodeCheckProp.CODE_CHECK_LISTVIEW_TITLE;
import static cc.CodeCheckProp.CODE_CHECK_PROGRESS_MESSAGE;
import static cc.CodeCheckProp.CODE_CHECK_RESULTS_BUTTON_MESSAGE;
import static cc.CodeCheckProp.CODE_PATH;
import static cc.CodeCheckProp.WORKSPACE_REFRESH_BUTTON_MESSAGE;
import static cc.CodeCheckProp.WORKSPACE_REMOVE_BUTTON_MESSAGE;
import static cc.CodeCheckProp.WORKSPACE_VIEW_BUTTON_MESSAGE;
import cc.data.CodeCheck;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_DEFAULT_BUTTONS;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_LISTVIEW;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_LISTVIEW_LABEL;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_MAIN_LABEL;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_PROGRESS_BAR;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_SECONDARY_LABEL;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_VIEW;
import javafx.beans.property.StringProperty;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

/**
 *
 * @author aespi
 */
public class CodeCheckView extends BorderPane{

    public ListView getCodeCheckSubListView() {
        return codeCheckSubListView;
    }
    
    CodeCheckApp app;
    CodeCheckController controller;
    
    private Label codeCheckSubLabel;
    private Label codeCheckDirectionsLabel;
    private Label progressLabel;
    private Label listViewLabel;
    private ListView codeCheckSubListView;
    private TextArea codeCheckSubTextArea;
    private Button codeCheckButton;
    private Button viewResultsButton;
    private Button removeSubButton;
    private Button refreshSubButton;
    private Button viewSubButton;
    private ProgressBar codeCheckProgressBar;
    private ProgressIndicator codeCheckProgressIndicator;
    private GridPane mainViewGridPane;
    private VBox tableAndButtonsVBox;
    private HBox buttonsHBox;
    private HBox progressHBox;
    private HBox codeCheckHBox;
    
    private String codeSubmissionsPath;
    
    public CodeCheckView(CodeCheckApp initApp){
        app = initApp;
        initLayout();
        initControllers();
        intiStyle();
        codeSubmissionsPath = PropertiesManager.getPropertiesManager().getProperty(CODE_PATH);
    }
    
    private void initLayout(){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        codeCheckSubLabel = new Label(props.getProperty(CODE_CHECK_LABEL_MESSAGE));
        codeCheckDirectionsLabel = new Label(props.getProperty(CODE_CHECK_DIRECTIONS_MESSAGE));
        progressLabel = new Label(props.getProperty(CODE_CHECK_PROGRESS_MESSAGE));
        listViewLabel = new Label(props.getProperty(CODE_CHECK_LISTVIEW_TITLE));
        codeCheckButton = new Button(props.getProperty(CODE_CHECK_BUTTON_MESSAGE));
        removeSubButton = new Button(props.getProperty(WORKSPACE_REMOVE_BUTTON_MESSAGE));
        refreshSubButton = new Button(props.getProperty(WORKSPACE_REFRESH_BUTTON_MESSAGE));
        viewSubButton = new Button(props.getProperty(WORKSPACE_VIEW_BUTTON_MESSAGE));
        viewResultsButton = new Button(props.getProperty(CODE_CHECK_RESULTS_BUTTON_MESSAGE));
        viewSubButton.setDisable(true);
        removeSubButton.setDisable(true);
        codeCheckButton.setDisable(true);
        viewResultsButton.setDisable(true);
        codeCheckSubListView = new ListView();
        codeCheckSubListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        codeCheckProgressBar = new ProgressBar();
        codeCheckProgressBar.setProgress(0.00F);
        
        codeCheckProgressIndicator = new ProgressIndicator();
        codeCheckProgressIndicator.setProgress(0.00);
        codeCheckSubTextArea = new TextArea();
        codeCheckSubTextArea.setEditable(false);
        codeCheckSubTextArea.setWrapText(true);
        
        buttonsHBox = new HBox(5);
        buttonsHBox.getChildren().add(removeSubButton);
        buttonsHBox.getChildren().add(refreshSubButton);
        buttonsHBox.getChildren().add(viewSubButton);
        
        progressHBox = new HBox(5);
        progressHBox.getChildren().add(progressLabel);
        progressHBox.getChildren().add(codeCheckProgressBar);
        progressHBox.getChildren().add(codeCheckProgressIndicator); 
        
        codeCheckHBox = new HBox(5);
        codeCheckHBox.getChildren().add(codeCheckButton);
        codeCheckHBox.getChildren().add(viewResultsButton);
        
        GridPane.setHgrow(this, Priority.ALWAYS);
        GridPane.setVgrow(this, Priority.ALWAYS);
        mainViewGridPane = new GridPane();
        mainViewGridPane.setVgap(10);
        mainViewGridPane.setHgap(50);
        //mainViewGridPane.setGridLinesVisible(true);
        mainViewGridPane.prefWidthProperty().bind(app.getGUI().getWindow().widthProperty().divide(1));
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(50);
        mainViewGridPane.getColumnConstraints().add(column1);
        
        mainViewGridPane.add(codeCheckSubLabel, 0, 0);
        mainViewGridPane.add(codeCheckDirectionsLabel, 0, 1);
        mainViewGridPane.add(listViewLabel, 0, 2);
        mainViewGridPane.add(codeCheckSubListView, 0, 3);
        mainViewGridPane.add(buttonsHBox, 0, 4);
        mainViewGridPane.add(progressHBox, 1, 0);
        mainViewGridPane.add(codeCheckHBox, 1, 1);
        mainViewGridPane.add(codeCheckSubTextArea, 1, 3);        
        
        this.setCenter(mainViewGridPane);
    }
        
    private void initControllers(){
        
        controller = new CodeCheckController(app);
        
        removeSubButton.setOnAction(e -> {
            controller.handleDirectoryRemove(codeCheckSubListView, codeSubmissionsPath);
            viewResultsButton.setDisable(true);
        });
        refreshSubButton.setOnAction(e -> controller.handleRefresh(codeCheckSubListView, codeSubmissionsPath));
        viewSubButton.setOnAction(e -> controller.handleDirectoryView(codeCheckSubListView, codeSubmissionsPath));
        codeCheckButton.setOnAction(e ->{
            controller.handleCodeCheck();
            viewResultsButton.setDisable(false);
        });
        viewResultsButton.setOnAction(e ->{
            controller.handleViewResults();
        });
        
        // Fool-proofing for the buttons
        codeCheckSubListView.setOnMouseClicked(e -> {
            Object itemSelected = codeCheckSubListView.getSelectionModel().getSelectedItem();
            int itemsSelected = codeCheckSubListView.getSelectionModel().getSelectedIndices().size();
            if (itemSelected != null && itemsSelected == 1) {
                viewSubButton.setDisable(false);
                removeSubButton.setDisable(false);
                codeCheckButton.setDisable(false);
                //viewResultsButton.setDisable(false);
            } else if (itemsSelected > 1) {
                viewSubButton.setDisable(true);
                removeSubButton.setDisable(true);
            }
        });
        this.setOnMouseClicked(e -> {
            codeCheckSubListView.getSelectionModel().clearSelection();
            viewSubButton.setDisable(true);
            removeSubButton.setDisable(true);
            codeCheckButton.setDisable(true);
            //viewResultsButton.setDisable(true);
        });
        codeCheckSubTextArea.setOnMouseClicked(e -> {
            codeCheckSubListView.getSelectionModel().clearSelection();
            viewSubButton.setDisable(true);
            removeSubButton.setDisable(true);
            codeCheckButton.setDisable(true);
            //viewResultsButton.setDisable(true);
        });
    }

    private void intiStyle() {
        this.getStyleClass().add(CLASS_WORKSPACE_VIEW);
        codeCheckSubLabel.getStyleClass().add(CLASS_WORKSPACE_MAIN_LABEL);
        codeCheckDirectionsLabel.getStyleClass().add(CLASS_WORKSPACE_SECONDARY_LABEL);
        listViewLabel.getStyleClass().add(CLASS_WORKSPACE_LISTVIEW_LABEL);
        codeCheckSubListView.getStyleClass().add(CLASS_WORKSPACE_LISTVIEW);
        progressLabel.getStyleClass().add(CLASS_WORKSPACE_MAIN_LABEL);
        removeSubButton.getStyleClass().add(CLASS_WORKSPACE_DEFAULT_BUTTONS);
        refreshSubButton.getStyleClass().add(CLASS_WORKSPACE_DEFAULT_BUTTONS);
        viewSubButton.getStyleClass().add(CLASS_WORKSPACE_DEFAULT_BUTTONS);
        codeCheckButton.getStyleClass().add(CLASS_WORKSPACE_DEFAULT_BUTTONS);
        codeCheckProgressBar.getStyleClass().add(CLASS_WORKSPACE_PROGRESS_BAR);
        viewResultsButton.getStyleClass().add(CLASS_WORKSPACE_DEFAULT_BUTTONS);
        viewResultsButton.setPadding(new Insets(5,35,5,35));
        mainViewGridPane.setPadding(new Insets(25,0,0,20));
        removeSubButton.setPadding(new Insets(5,35,5,35));
        refreshSubButton.setPadding(new Insets(5,35,5,35));
        viewSubButton.setPadding(new Insets(5,35,5,35));
        codeCheckButton.setPadding(new Insets(5,35,5,35));
    }    
    
    public TextArea getCodeCheckSubTextArea() {
        return codeCheckSubTextArea;
    }

    public ProgressBar getCodeCheckProgressBar() {
        return codeCheckProgressBar;
    }

    public ProgressIndicator getCodeCheckProgressIndicator() {
        return codeCheckProgressIndicator;
    }
    
}
