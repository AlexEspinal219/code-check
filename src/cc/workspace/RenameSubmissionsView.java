/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.workspace;

import cc.CodeCheckApp;
import static cc.CodeCheckProp.RENAME_SUBMISSIONS_BUTTON_MESSAGE;
import static cc.CodeCheckProp.RENAME_SUBMISSIONS_DIRECTIONS_MESSAGE;
import static cc.CodeCheckProp.RENAME_SUBMISSIONS_LABEL_MESSAGE;
import static cc.CodeCheckProp.RENAME_SUBMISSIONS_LISTVIEW_TITLE;
import static cc.CodeCheckProp.RENAME_SUBMISSIONS_PROGRESS_MESSAGE;
import static cc.CodeCheckProp.SUBMISSIONS_PATH;
import static cc.CodeCheckProp.WORKSPACE_REFRESH_BUTTON_MESSAGE;
import static cc.CodeCheckProp.WORKSPACE_REMOVE_BUTTON_MESSAGE;
import static cc.CodeCheckProp.WORKSPACE_VIEW_BUTTON_MESSAGE;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_DEFAULT_BUTTONS;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_LISTVIEW;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_LISTVIEW_LABEL;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_MAIN_LABEL;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_PROGRESS_BAR;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_SECONDARY_LABEL;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_VIEW;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

/**
 *
 * @author aespi
 */
public class RenameSubmissionsView extends BorderPane {

    public ListView getRenameSubListView() {
        return renameSubListView;
    }

    CodeCheckApp app;
    CodeCheckController controller;

    private Label renameSubLabel;
    private Label renameDirectionsLabel;
    private Label progressLabel;
    private Label listViewLabel;
    private ListView renameSubListView;
    private TextArea renameSubTextArea;
    private Button renameButton;
    private Button removeSubButton;
    private Button refreshSubButton;
    private Button viewSubButton;
    private ProgressBar renameProgressBar;
    private ProgressIndicator renameProgressIndicator;
    private GridPane mainViewGridPane;
    private HBox buttonsHBox;
    private HBox progressHBox;

    private String submissionsPath;

    public RenameSubmissionsView(CodeCheckApp initApp) {
        app = initApp;
        initLayout();
        initControllers();
        initStyle();

        submissionsPath = PropertiesManager.getPropertiesManager().getProperty(SUBMISSIONS_PATH);

    }

    private void initLayout() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        renameSubLabel = new Label(props.getProperty(RENAME_SUBMISSIONS_LABEL_MESSAGE));
        renameDirectionsLabel = new Label(props.getProperty(RENAME_SUBMISSIONS_DIRECTIONS_MESSAGE));
        progressLabel = new Label(props.getProperty(RENAME_SUBMISSIONS_PROGRESS_MESSAGE));
        listViewLabel = new Label(props.getProperty(RENAME_SUBMISSIONS_LISTVIEW_TITLE));
        renameButton = new Button(props.getProperty(RENAME_SUBMISSIONS_BUTTON_MESSAGE));
        removeSubButton = new Button(props.getProperty(WORKSPACE_REMOVE_BUTTON_MESSAGE));
        refreshSubButton = new Button(props.getProperty(WORKSPACE_REFRESH_BUTTON_MESSAGE));
        viewSubButton = new Button(props.getProperty(WORKSPACE_VIEW_BUTTON_MESSAGE));
        viewSubButton.setDisable(true);
        removeSubButton.setDisable(true);
        renameButton.setDisable(true);
        renameSubListView = new ListView();
        renameSubListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        renameProgressBar = new ProgressBar();
        renameProgressBar.setProgress(0.00F);
        
        renameProgressIndicator = new ProgressIndicator();
        renameProgressIndicator.setProgress(0.00F);
        renameSubTextArea = new TextArea();
        renameSubTextArea.setEditable(false);
        renameSubTextArea.setWrapText(true);
        
        buttonsHBox = new HBox(5);
        buttonsHBox.getChildren().add(removeSubButton);
        buttonsHBox.getChildren().add(refreshSubButton);
        buttonsHBox.getChildren().add(viewSubButton);

        progressHBox = new HBox(5);
        progressHBox.getChildren().add(progressLabel);
        progressHBox.getChildren().add(renameProgressBar);
        progressHBox.getChildren().add(renameProgressIndicator);

        GridPane.setHgrow(this, Priority.ALWAYS);
        GridPane.setVgrow(this, Priority.ALWAYS);
        mainViewGridPane = new GridPane();
        mainViewGridPane.setVgap(10);
        mainViewGridPane.setHgap(50);
        //mainViewGridPane.setGridLinesVisible(true);
        mainViewGridPane.prefWidthProperty().bind(app.getGUI().getWindow().widthProperty().divide(1));
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(50);
        mainViewGridPane.getColumnConstraints().add(column1);

        mainViewGridPane.add(renameSubLabel, 0, 0);
        mainViewGridPane.add(renameDirectionsLabel, 0, 1);
        mainViewGridPane.add(listViewLabel, 0, 2);
        mainViewGridPane.add(renameSubListView, 0, 3);
        mainViewGridPane.add(buttonsHBox, 0, 4);
        mainViewGridPane.add(progressHBox, 1, 0);
        mainViewGridPane.add(renameButton, 1, 1);
        mainViewGridPane.add(renameSubTextArea, 1, 3);

        this.setCenter(mainViewGridPane);
    }

    private void initControllers() {
        controller = new CodeCheckController(app);

        refreshSubButton.setOnAction(e -> {
            controller.handleRefresh(renameSubListView, submissionsPath);
            if (!renameSubListView.getItems().isEmpty()) {                
                renameButton.setDisable(false);
            }else{
                renameButton.setDisable(true);
                renameProgressBar.setProgress(0.00F);
                renameProgressIndicator.setProgress(0.00F);
            }
        });

        viewSubButton.setOnAction(e -> controller.handleZipView(renameSubListView, submissionsPath));
        removeSubButton.setOnAction(e -> {
            controller.handleZipRemove(renameSubListView, submissionsPath);
        });
        renameButton.setOnAction(e -> {
            controller.handleRename(renameSubListView, submissionsPath);
            
            if (renameSubListView.getItems().isEmpty()) {
                renameButton.setDisable(true);
                removeSubButton.setDisable(true);
                renameButton.setDisable(true);
            }
            controller.handleRefresh(renameSubListView, submissionsPath);
        });

        // Fool-proofing for the buttons
        renameSubListView.setOnMouseClicked(e -> {
            Object itemSelected = renameSubListView.getSelectionModel().getSelectedItem();
            int itemsSelected = renameSubListView.getSelectionModel().getSelectedIndices().size();
            if (itemSelected != null && itemsSelected == 1) {
                viewSubButton.setDisable(false);
                removeSubButton.setDisable(false);
            } else if (itemsSelected > 1) {
                viewSubButton.setDisable(true);
                removeSubButton.setDisable(true);
            }
        });
        this.setOnMouseClicked(e -> {
            renameSubListView.getSelectionModel().clearSelection();
            viewSubButton.setDisable(true);
            removeSubButton.setDisable(true);
        });
        renameSubTextArea.setOnMouseClicked(e -> {
            renameSubListView.getSelectionModel().clearSelection();
            viewSubButton.setDisable(true);
            removeSubButton.setDisable(true);
        });
    }


    private void initStyle() {
        this.getStyleClass().add(CLASS_WORKSPACE_VIEW);
        renameSubLabel.getStyleClass().add(CLASS_WORKSPACE_MAIN_LABEL);
        renameDirectionsLabel.getStyleClass().add(CLASS_WORKSPACE_SECONDARY_LABEL);
        listViewLabel.getStyleClass().add(CLASS_WORKSPACE_LISTVIEW_LABEL);
        renameSubListView.getStyleClass().add(CLASS_WORKSPACE_LISTVIEW);
        progressLabel.getStyleClass().add(CLASS_WORKSPACE_MAIN_LABEL);
        removeSubButton.getStyleClass().add(CLASS_WORKSPACE_DEFAULT_BUTTONS);
        refreshSubButton.getStyleClass().add(CLASS_WORKSPACE_DEFAULT_BUTTONS);
        viewSubButton.getStyleClass().add(CLASS_WORKSPACE_DEFAULT_BUTTONS);
        renameButton.getStyleClass().add(CLASS_WORKSPACE_DEFAULT_BUTTONS);
        renameProgressBar.getStyleClass().add(CLASS_WORKSPACE_PROGRESS_BAR);
        mainViewGridPane.setPadding(new Insets(25, 0, 0, 20));
        removeSubButton.setPadding(new Insets(5, 35, 5, 35));
        refreshSubButton.setPadding(new Insets(5, 35, 5, 35));
        viewSubButton.setPadding(new Insets(5, 35, 5, 35));
        renameButton.setPadding(new Insets(5, 35, 5, 35));
    }
    
    // Getters
    public TextArea getRenameSubTextArea() {
        return renameSubTextArea;
    }

    public ProgressBar getRenameProgressBar() {
        return renameProgressBar;
    }

    public ProgressIndicator getRenameProgressIndicator() {
        return renameProgressIndicator;
    }

    public Button getRenameButton() {
        return renameButton;
    }
}
