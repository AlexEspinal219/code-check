/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.workspace;

import cc.CodeCheckApp;
import static cc.CodeCheckProp.BLACKBOARD_PATH;
import static cc.CodeCheckProp.EXTRACT_SUBMISSIONS_BUTTON_MESSAGE;
import static cc.CodeCheckProp.EXTRACT_SUBMISSIONS_DIRECTIONS_MESSAGE;
import static cc.CodeCheckProp.EXTRACT_SUBMISSIONS_LABEL_MESSAGE;
import static cc.CodeCheckProp.EXTRACT_SUBMISSIONS_LISTVIEW_TITLE;
import static cc.CodeCheckProp.EXTRACT_SUBMISSIONS_PROGRESS_MESSAGE;
import static cc.CodeCheckProp.SUBMISSIONS_PATH;
import static cc.CodeCheckProp.WORKSPACE_REFRESH_BUTTON_MESSAGE;
import static cc.CodeCheckProp.WORKSPACE_REMOVE_BUTTON_MESSAGE;
import static cc.CodeCheckProp.WORKSPACE_VIEW_BUTTON_MESSAGE;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_DEFAULT_BUTTONS;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_LISTVIEW;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_LISTVIEW_LABEL;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_MAIN_LABEL;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_PROGRESS_BAR;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_SECONDARY_LABEL;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_VIEW;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import properties_manager.PropertiesManager;

/**
 *
 * @author aespi
 */
public class ExtractSubmissionView extends BorderPane {

    CodeCheckApp app;
    CodeCheckController controller;

    private Label extractSubLabel;
    private Label extractDirectionsLabel;
    private Label progressLabel;
    private Label listViewLabel;
    private ListView extractSubListView;
    private TextArea extractSubTextArea;
    private Button extractButton;
    private Button removeSubButton;
    private Button refreshSubButton;
    private Button viewSubButton;
    private ProgressBar extractProgressBar;
    private ProgressIndicator extractProgressIndicator;
    private GridPane mainViewGridPane;
    private HBox buttonsHBox;
    private HBox progressHBox;

    private String blackboardPath;
    private String submissionsPath;

    public ExtractSubmissionView(CodeCheckApp initApp) {
        app = initApp;
        initLayout();
        initControllers();
        initStyle();

        blackboardPath = PropertiesManager.getPropertiesManager().getProperty(BLACKBOARD_PATH);
        submissionsPath = PropertiesManager.getPropertiesManager().getProperty(SUBMISSIONS_PATH);
    }

    private void initLayout() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        extractSubLabel = new Label(props.getProperty(EXTRACT_SUBMISSIONS_LABEL_MESSAGE));
        extractDirectionsLabel = new Label(props.getProperty(EXTRACT_SUBMISSIONS_DIRECTIONS_MESSAGE));
        progressLabel = new Label(props.getProperty(EXTRACT_SUBMISSIONS_PROGRESS_MESSAGE));
        listViewLabel = new Label(props.getProperty(EXTRACT_SUBMISSIONS_LISTVIEW_TITLE));
        extractButton = new Button(props.getProperty(EXTRACT_SUBMISSIONS_BUTTON_MESSAGE));
        removeSubButton = new Button(props.getProperty(WORKSPACE_REMOVE_BUTTON_MESSAGE));
        refreshSubButton = new Button(props.getProperty(WORKSPACE_REFRESH_BUTTON_MESSAGE));
        viewSubButton = new Button(props.getProperty(WORKSPACE_VIEW_BUTTON_MESSAGE));
        viewSubButton.setDisable(true);
        removeSubButton.setDisable(true);
        extractButton.setDisable(true);
        extractSubListView = new ListView();
        extractSubListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        extractProgressBar = new ProgressBar();
        extractProgressBar.setProgress(0.00F);

        extractProgressIndicator = new ProgressIndicator();
        extractProgressIndicator.setProgress(0.00F);
        extractSubTextArea = new TextArea();
        extractSubTextArea.setEditable(false);

        buttonsHBox = new HBox(5);
        buttonsHBox.getChildren().add(removeSubButton);
        buttonsHBox.getChildren().add(refreshSubButton);
        buttonsHBox.getChildren().add(viewSubButton);

        progressHBox = new HBox(5);
        progressHBox.getChildren().add(progressLabel);
        progressHBox.getChildren().add(extractProgressBar);
        progressHBox.getChildren().add(extractProgressIndicator);

        GridPane.setHgrow(this, Priority.ALWAYS);
        GridPane.setVgrow(this, Priority.ALWAYS);
        mainViewGridPane = new GridPane();
        mainViewGridPane.setVgap(10);
        mainViewGridPane.setHgap(50);
        //mainViewGridPane.setGridLinesVisible(true);
        mainViewGridPane.prefWidthProperty().bind(app.getGUI().getWindow().widthProperty().divide(1));
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(50);
        mainViewGridPane.getColumnConstraints().add(column1);

        mainViewGridPane.add(extractSubLabel, 0, 0);
        mainViewGridPane.add(extractDirectionsLabel, 0, 1);
        mainViewGridPane.add(listViewLabel, 0, 2);
        mainViewGridPane.add(extractSubListView, 0, 3);
        mainViewGridPane.add(buttonsHBox, 0, 4);
        mainViewGridPane.add(progressHBox, 1, 0);
        mainViewGridPane.add(extractButton, 1, 1);
        mainViewGridPane.add(extractSubTextArea, 1, 3);

        this.setCenter(mainViewGridPane);

    }

    private void initControllers() {
        controller = new CodeCheckController(app);

        refreshSubButton.setOnAction(e -> controller.handleRefresh(extractSubListView, blackboardPath));
        viewSubButton.setOnAction(e -> controller.handleZipView(extractSubListView, blackboardPath));
        removeSubButton.setOnAction(e -> controller.handleZipRemove(extractSubListView, blackboardPath));
        extractButton.setOnAction(e -> controller.handleBlackboardSubmissions(extractSubListView, blackboardPath, submissionsPath));
        
        // Fool-proofing for the buttons
        extractSubListView.setOnMouseClicked(e -> {
            Object itemSelected = extractSubListView.getSelectionModel().getSelectedItem();
            int itemsSelected = extractSubListView.getSelectionModel().getSelectedIndices().size();
            if (itemSelected != null && itemsSelected == 1) {
                viewSubButton.setDisable(false);
                removeSubButton.setDisable(false);
                extractButton.setDisable(false);
            } else if (itemsSelected > 1) {
                viewSubButton.setDisable(true);
                removeSubButton.setDisable(true);
            }
        });
        this.setOnMouseClicked(e -> {
            extractSubListView.getSelectionModel().clearSelection();
            viewSubButton.setDisable(true);
            removeSubButton.setDisable(true);
            extractButton.setDisable(true);
        });
        extractSubTextArea.setOnMouseClicked(e -> {
            extractSubListView.getSelectionModel().clearSelection();
            viewSubButton.setDisable(true);
            removeSubButton.setDisable(true);
            extractButton.setDisable(true);
        });
    }

    private void initStyle() {
        this.getStyleClass().add(CLASS_WORKSPACE_VIEW);
        extractSubLabel.getStyleClass().add(CLASS_WORKSPACE_MAIN_LABEL);
        extractDirectionsLabel.getStyleClass().add(CLASS_WORKSPACE_SECONDARY_LABEL);
        listViewLabel.getStyleClass().add(CLASS_WORKSPACE_LISTVIEW_LABEL);
        extractSubListView.getStyleClass().add(CLASS_WORKSPACE_LISTVIEW);
        progressLabel.getStyleClass().add(CLASS_WORKSPACE_MAIN_LABEL);
        removeSubButton.getStyleClass().add(CLASS_WORKSPACE_DEFAULT_BUTTONS);
        refreshSubButton.getStyleClass().add(CLASS_WORKSPACE_DEFAULT_BUTTONS);
        viewSubButton.getStyleClass().add(CLASS_WORKSPACE_DEFAULT_BUTTONS);
        extractButton.getStyleClass().add(CLASS_WORKSPACE_DEFAULT_BUTTONS);
        extractProgressBar.getStyleClass().add(CLASS_WORKSPACE_PROGRESS_BAR);
        mainViewGridPane.setPadding(new Insets(25, 0, 0, 20));
        removeSubButton.setPadding(new Insets(5, 35, 5, 35));
        refreshSubButton.setPadding(new Insets(5, 35, 5, 35));
        viewSubButton.setPadding(new Insets(5, 35, 5, 35));
        extractButton.setPadding(new Insets(5, 35, 5, 35));
    }

    public ListView getExtractSubListView() {
        return extractSubListView;
    }

    public ProgressBar getBlackboardSubmissionProgressBar() {
        return extractProgressBar;
    }

    public ProgressIndicator getBlackboardSubmissionProgressIndicator() {
        return extractProgressIndicator;
    }

    public TextArea getExtractSubTextArea() {
        return extractSubTextArea;
    }

}
