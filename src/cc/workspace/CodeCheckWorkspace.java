/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.workspace;

import cc.CodeCheckApp;
import static cc.CodeCheckProp.ABOUT_ICON;
import static cc.CodeCheckProp.ABOUT_TOOLTIP;
import static cc.CodeCheckProp.CLOSE_DIALOG_CONTENT;
import static cc.CodeCheckProp.CLOSE_DIALOG_HEADER;
import static cc.CodeCheckProp.CLOSE_DIALOG_TITLE;
import static cc.CodeCheckProp.HOME_ICON;
import static cc.CodeCheckProp.HOME_TOOLTIP;
import static cc.CodeCheckProp.LOAD_TOOLTIP;
import static cc.CodeCheckProp.NEXT_ICON;
import static cc.CodeCheckProp.NEXT_TOOLTIP;
import static cc.CodeCheckProp.PREVIOUS_ICON;
import static cc.CodeCheckProp.PREVIOUS_TOOLTIP;
import static cc.CodeCheckProp.RENAME_ICON;
import static cc.CodeCheckProp.RENAME_TOOLTIP;
import static cc.style.CodeCheckStyle.CLASS_NAVIGATION_PANE;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import static djf.settings.AppPropertyType.LOAD_ICON;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import static djf.ui.AppGUI.CLASS_FILE_BUTTON;
import java.util.Optional;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextArea;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.WindowEvent;
import properties_manager.PropertiesManager;

/**
 *
 * @author aespi
 */
public class CodeCheckWorkspace extends AppWorkspaceComponent {

    CodeCheckApp app;
    CodeCheckController controller;
    
    // Buttons we'll add to the same toolbar as new and load.
    private Button loadButton;
    private Button renameButton;
    private Button aboutButton;
    // Toolbar and buttons we'll add to the TopToolBar in the AppGUI
    private HBox navigationToolbar;
    private Button homeButton;
    private Button previousButton;
    private Button nextButton;
    
    private boolean loading;
    // In charge of the different views
    WorkspaceFlow workflow;
    
    // We'll use this to help keep track of which view we're currently on.
    // MAYBE THINK OF DECLARING FINAL CONSTANTS INSTEAD OF JUST ONE INT
    private int currentView;
    private int numCloses;
    
    public CodeCheckWorkspace(CodeCheckApp initApp) {
        app = initApp;
        workflow = new WorkspaceFlow(app);
        currentView = 0;
        numCloses = 0;
        
        hideTopToolbarPane();
        BorderPane workspaceBorderPane;
        workspaceBorderPane = workflow.getWelcomeView();
        workspace = workspaceBorderPane;
        app.getGUI().getAppPane().setCenter(workspace);
        initLayout();
        initControllers();
        initStyle();
       
    }
        
    private void initLayout(){
        // While the DJF does provide some of the buttons that we need, we 
        // need to implement our own additional buttons. 2 in the already
        // created filetool bar and 3 more in an additional toolbar we'll
        // create then add to the ToolBarPane in the AppGUI.
        
        // Add the rename and about buttons on the left toolbar
        FlowPane fileToolbar = app.getGUI().getFileToolbar();
        loadButton = initChildButton(fileToolbar, LOAD_ICON.toString(), LOAD_TOOLTIP.toString(), false);
        renameButton = initChildButton(fileToolbar, RENAME_ICON.toString(), RENAME_TOOLTIP.toString(), true);
        aboutButton = initChildButton(fileToolbar, ABOUT_ICON.toString(), ABOUT_TOOLTIP.toString(), false);
        
        // Add the three navigation buttons to the toolbar we declared and 
        // place it to the right.
        navigationToolbar = new HBox();
        homeButton = initChildButton(navigationToolbar, HOME_ICON.toString(), HOME_TOOLTIP.toString(), true);
        previousButton = initChildButton(navigationToolbar, PREVIOUS_ICON.toString(), PREVIOUS_TOOLTIP.toString(), true);
        nextButton = initChildButton(navigationToolbar, NEXT_ICON.toString(), NEXT_TOOLTIP.toString(), true);
        app.getGUI().getTopToolbarPane().getChildren().add(navigationToolbar);
        
    }
    
    private Button initChildButton(Pane toolbar, String icon, String tooltip, boolean disabled){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // LOAD THE ICON FROM THE PROVIDED FILE
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(icon);
        Image buttonImage = new Image(imagePath);
	
	// NOW MAKE THE BUTTON
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip));
        button.setTooltip(buttonTooltip);
	
	// PUT THE BUTTON IN THE TOOLBAR
        toolbar.getChildren().add(button);
	
	// AND RETURN THE COMPLETED BUTTON
        return button;
    }
    
    private void initControllers(){
        controller = new CodeCheckController(app);
        
        renameButton.setOnAction(e-> controller.handleRename());
        aboutButton.setOnAction(e -> controller.handleAbout());
        homeButton.setOnAction(e -> controller.handleHomeView());
        previousButton.setOnAction(e -> controller.handlePrevView());
        nextButton.setOnAction(e -> controller.handleNextView());
        loadButton.setOnAction(e -> controller.handleLoad());
       
        // Use this to get the window now to close
        Platform.setImplicitExit(false);    // Set this to true after the welcome dialog
        app.getGUI().getWindow().setOnCloseRequest((WindowEvent event) -> {
            event.consume();
            //System.out.println("currentview:" + currentView);
            if(currentView == 0 && numCloses == 0){
                app.getGUI().getAppPane().setCenter(new BorderPane());
                showTopToolbarPane();
                numCloses++;
            } else if( numCloses >= 0) {
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                Alert alert = new Alert(AlertType.CONFIRMATION);
                alert.setTitle(props.getProperty(CLOSE_DIALOG_TITLE));
                alert.setHeaderText(props.getProperty(CLOSE_DIALOG_HEADER));
                alert.setContentText(props.getProperty(CLOSE_DIALOG_CONTENT));
                
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK){
                    System.exit(0);
                }
            }            
        }); 
    }
    
    private void initStyle(){
        navigationToolbar.getStyleClass().add(CLASS_NAVIGATION_PANE);        
        renameButton.getStyleClass().add(CLASS_FILE_BUTTON);
        aboutButton.getStyleClass().add(CLASS_FILE_BUTTON);
        homeButton.getStyleClass().add(CLASS_FILE_BUTTON);
        previousButton.getStyleClass().add(CLASS_FILE_BUTTON);
        nextButton.getStyleClass().add(CLASS_FILE_BUTTON);
        loadButton.getStyleClass().add(CLASS_FILE_BUTTON);
    }
    
    private void hideTopToolbarPane(){
        app.getGUI().getTopToolbarPane().setVisible(false);
    }
    
    public void showTopToolbarPane(){
        app.getGUI().getTopToolbarPane().setVisible(true);
    }
    
    public CodeCheckController getController(){
        return controller;
    }
    
    public WorkspaceFlow getWorkflow(){
        return workflow;
    }
    
    public int getCurrentViewIndex(){
        return currentView;
    }
    
    public void incCurrentView(){
        currentView++;
    }
    
    public void decCurrentView(){
        currentView--;
    }
    
    public void setCurrentViewToHome(){
        currentView = 1;
    }
    
    @Override
    public void resetWorkspace() {
        loading = true;
        // set title to default
        app.getGUI().getWindow().setTitle("");
        //app.getGUI().getAppPane().setCenter(new BorderPane());
        workflow = new WorkspaceFlow(app);
        setCurrentViewToHome();
        workspace = workflow.getHomeView();
        controller.handleNewCodeCheck();        
        
    }

    public boolean isLoading() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        
    }
    
    
    // Workspace toolbar buttons
    public Button getRenameButton() {
        return renameButton;
    }

    public Button getAboutButton() {
        return aboutButton;
    }

    public Button getHomeButton() {
        return homeButton;
    }

    public Button getPreviousButton() {
        return previousButton;
    }

    public Button getNextButton() {
        return nextButton;
    }
    
    // Getters for controls from the views
    
    
    // Step 1
    public ListView getExtractSubListView(){
        return workflow.getExtractSubListView();
    }
    
    public ProgressBar getExtractSubProgressBar(){
        return workflow.getExtractSubProgressBar();
    }
    
    public ProgressIndicator getExtractProgressIndicator(){
        return workflow.getExtractProgressIndicator();
    }
    
    public TextArea getExtractSubTextArea() {
        return workflow.getExtractSubTextArea();
    }
        
    // Step 2
    public ListView getRenameSubListView(){
        return workflow.getRenameSubListView();
    }
    
    public TextArea getRenameSubTextArea() {
        return workflow.getRenameSubTextArea();
    }

    public ProgressBar getRenameProgressBar() {
        return workflow.getRenameProgressBar();
    }

    public ProgressIndicator getRenameProgressIndicator() {
        return workflow.getRenameProgressIndicator();
    }

    public Button getRenameSubButton() {
        return workflow.getRenameButton();
    }
    
    // Step 3
    public ListView getUnzipSubListView(){
        return workflow.getUnzipSubListView();
    }
    
    public TextArea getUnzipSubTextArea() {
        return workflow.getUnzipSubTextArea();
    }

    public ProgressBar getUnzipProgressBar() {
        return workflow.getUnzipProgressBar();
    }

    public ProgressIndicator getUnzipProgressIndicator() {
        return workflow.getUnzipProgressIndicator();
    }
    
    // Step 4
    public ListView getExtractSrcListView(){
        return  workflow.getExtractSrcListView();
    }
            
    public ProgressBar getExtractSourceCodeProgressBar() {
        return workflow.getExtractSourceCodeProgressBar();
    }

    public ProgressIndicator getSourceCodeProgressIndicator() {
        return workflow.getSourceCodeProgressIndicator();
    }
    
    public boolean extractJava(){
        return workflow.extractJava();
    }
    
    public boolean extractJS(){
        return workflow.extractJS();
    }
    
    public boolean extractC(){
        return workflow.extractC();
    }
    
    public boolean extractCS(){
        return workflow.extractCS();
    }
    
    public boolean extractOther(){
        return workflow.extractOther();
    }
    
    public String getOtherExtension() {
        return workflow.getOtherExtension();
    }
    
    public TextArea getExtractSourceTextArea(){
        return workflow.getExtractSourceTextArea();
    }
    
    // Step 5
    public ListView getCodeCheckListView(){
        return  workflow.getCodeCheckListView();
    }    
    
    public TextArea getCodeCheckSubTextArea() {
        return workflow.getCodeCheckSubTextArea();
    }
    
    public ProgressBar getCodeCheckProgressBar() {
        return workflow.getCodeCheckProgressBar();
    }

    public ProgressIndicator getCodeCheckProgressIndicator() {
        return workflow.getCodeCheckProgressIndicator();
    }
    
}
