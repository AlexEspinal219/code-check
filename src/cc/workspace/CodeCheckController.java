/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.workspace;

import cc.CodeCheckApp;
import static cc.CodeCheckProp.ABOUT_MESSAGE;
import static cc.CodeCheckProp.ABOUT_TITLE;
import static cc.CodeCheckProp.BLACKBOARD_PATH;
import static cc.CodeCheckProp.CODE_CHECK_INVALID_NAME_ERROR_MESSAGE;
import static cc.CodeCheckProp.CODE_CHECK_LOADING_ERROR_MESSAGE;
import static cc.CodeCheckProp.CODE_CHECK_LOADING_ERROR_TITLE;
import static cc.CodeCheckProp.CODE_CHECK_NAME_ERROR_MESSAGE;
import static cc.CodeCheckProp.CODE_CHECK_NAME_ERROR_TITLE;
import static cc.CodeCheckProp.CODE_PATH;
import static cc.CodeCheckProp.CONTENT_TEXT_DIALOG_BOX_TITLE;
import static cc.CodeCheckProp.EXTRACTION_FAILED;
import static cc.CodeCheckProp.EXTRACTION_SUCCESSFUL;
import static cc.CodeCheckProp.PROJECTS_PATH;
import static cc.CodeCheckProp.SUBMISSIONS_PATH;
import static cc.CodeCheckProp.TITLE_PREFIX;
import static cc.CodeCheckProp.TITLE_TEXT_DIALOG_BOX_TITLE;
import cc.data.CodeCheckData;
import cc.file.CodeCheckFiles;
import static djf.settings.AppStartupConstants.PATH_WORK;
import djf.ui.AppMessageDialogSingleton;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.BorderPane;
import javafx.scene.web.WebView;
import javafx.scene.web.WebEngine;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.progress.ProgressMonitor;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author aespi
 */
public class CodeCheckController {

    CodeCheckApp app;
    private final String ZIPEXTENSION = ".zip";
    private final String BACKSLASH = "\\";
    private final String ILLEGALCHARS = "[~#@*+%{}<>\\[\\]|\"\\_^]";
    ReentrantLock progressLock;
    int numTasks = 0;

    /**
     * Constructor, note that the app must already be constructed.
     *
     * @param initApp
     */
    CodeCheckController(CodeCheckApp initApp) {
        app = initApp;
        progressLock = new ReentrantLock();
    }

    public void handleNextView() {
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        workspace.showTopToolbarPane();
        WorkspaceFlow workflow = workspace.getWorkflow();
        workspace.incCurrentView();
        int viewCounter = workspace.getCurrentViewIndex();
        app.getGUI().getAppPane().setCenter(workflow.getView(viewCounter));
        setButtons();

    }

    public void handlePrevView() {
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        WorkspaceFlow workflow = workspace.getWorkflow();
        workspace.decCurrentView();
        int viewCounter = workspace.getCurrentViewIndex();
        app.getGUI().getAppPane().setCenter(workflow.getView(viewCounter));
        setButtons();
    }

    private void setButtons() {
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        WorkspaceFlow workflow = workspace.getWorkflow();
        int viewCounter = workspace.getCurrentViewIndex();

        // Three scenearios need to be accounted for with regards to viewCounter.
        // 1) At home view
        // 2) At last view
        // 3) At any of the other views
        if (viewCounter == 1) {
            workspace.getNextButton().setDisable(false);
            workspace.getPreviousButton().setDisable(true);
            workspace.getHomeButton().setDisable(true);
            workspace.getAboutButton().setDisable(false);
            workspace.getRenameButton().setDisable(false);
        } else if (viewCounter == workflow.size() - 1) {
            workspace.getNextButton().setDisable(true);
            workspace.getHomeButton().setDisable(false);
        } else {
            workspace.getPreviousButton().setDisable(false);
            workspace.getNextButton().setDisable(false);
            workspace.getHomeButton().setDisable(false);
        }
    }

    private void setView(BorderPane currentView) {
        app.getGUI().getAppPane().setCenter(currentView);
    }

    public void handleHomeView() {
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        WorkspaceFlow workflow = workspace.getWorkflow();

        workspace.setCurrentViewToHome();
        int viewCounter = workspace.getCurrentViewIndex();

        setView(workflow.getView(viewCounter));
        setButtons();
    }

    public void handleRename() {
        CodeCheckData dataManager = (CodeCheckData) app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        TextInputDialog dialog = new TextInputDialog("");
        dialog.setTitle(props.getProperty(TITLE_TEXT_DIALOG_BOX_TITLE));
        dialog.setHeaderText("");
        dialog.setContentText(props.getProperty(CONTENT_TEXT_DIALOG_BOX_TITLE));
        Optional<String> result = dialog.showAndWait();
        result.ifPresent(name -> {
            // Use if statement from handleNewCodeCheck
            if ((new File(PATH_WORK + name)).exists()) {
                // PROMPT THE USER WITH A MESSAGE SAYING THAT DIRECTORY EXIST
                promptMessage(CODE_CHECK_NAME_ERROR_TITLE.toString(),
                        CODE_CHECK_NAME_ERROR_MESSAGE.toString());
                handleRename();
            } else {
                if (!validName(name)) {
                    File dirToRename = new File(PATH_WORK + dataManager.getCodeCheckName());
                    File renamedDir = new File(PATH_WORK + name);
                    dirToRename.renameTo(renamedDir);
                    setCodeCheckName(name);
                } else {
                    promptMessage(CODE_CHECK_NAME_ERROR_TITLE.toString(),
                            CODE_CHECK_INVALID_NAME_ERROR_MESSAGE.toString());
                    handleRename();
                }
            }
        });
    }

    public void handleAbout() {
        promptMessage(ABOUT_TITLE.toString(), ABOUT_MESSAGE.toString());
    }

    public void handleNewCodeCheck() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        TextInputDialog dialog = new TextInputDialog("");
        dialog.setTitle(props.getProperty(TITLE_TEXT_DIALOG_BOX_TITLE));
        dialog.setHeaderText("");
        dialog.setContentText(props.getProperty(CONTENT_TEXT_DIALOG_BOX_TITLE));
        Optional<String> result = dialog.showAndWait();

        result.ifPresent(name -> {
            // Instantiates the code check object and creates the directory with its subdirectories.
            if ((new File(PATH_WORK + name)).exists()) {
                // PROMPT THE USER WITH A MESSAGE SAYING THAT DIRECTORY EXIST
                promptMessage(CODE_CHECK_NAME_ERROR_TITLE.toString(),
                        CODE_CHECK_NAME_ERROR_MESSAGE.toString());
                handleNewCodeCheck();
            } else {
                if (!validName(name)) {
                    setCodeCheckName(name);
                    initializeCodeCheck(name);
                    CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
                    handleHomeView();
                    workspace.showTopToolbarPane();
                    setButtons();
                } else {
                    promptMessage(CODE_CHECK_NAME_ERROR_TITLE.toString(),
                            CODE_CHECK_INVALID_NAME_ERROR_MESSAGE.toString());
                    handleNewCodeCheck();
                }
            }
        });

    }

    private void promptMessage(String promptTitle, String promptMessage) {
        CodeCheckData dataManager = (CodeCheckData) app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String title = props.getProperty(promptTitle);
        String message = props.getProperty(promptMessage);
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        dialog.show(title, message);// + "\nCode Check Name: " + dataManager.getCodeCheckName());
    }

    private void promptMessage2(String title, String message) {
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        dialog.show(title, message);
    }

    private void setCodeCheckName(String codeCheckName) {
        CodeCheckData dataManager = (CodeCheckData) app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        app.getGUI().getWindow().setTitle(props.getProperty(TITLE_PREFIX) + codeCheckName);
        dataManager.setCodeCheckName(codeCheckName);
        dataManager.setCodeCheckAddress(PATH_WORK + codeCheckName);
        System.out.println(dataManager.getCodeCheckAddress());
        CodeCheckFiles fileManager = (CodeCheckFiles) app.getFileComponent();

    }

    private void initializeCodeCheck(String name) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        // Set the name and initialize the directories.        
        String newDirectory = PATH_WORK + name;
        File dir = new File(newDirectory);
        boolean success = dir.mkdir();
        if (success) {
            File blackboardDir = new File(newDirectory + props.getProperty(BLACKBOARD_PATH));
            File submissionsDir = new File(newDirectory + props.getProperty(SUBMISSIONS_PATH));
            File projectsDir = new File(newDirectory + props.getProperty(PROJECTS_PATH));
            File codeDir = new File(newDirectory + props.getProperty(CODE_PATH));
            blackboardDir.mkdir();
            submissionsDir.mkdir();
            projectsDir.mkdir();
            codeDir.mkdir();
        } else {
            // Prompt the user with an error message possibly with feedback

        }
    }

    public void handleLoad() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        DirectoryChooser dirChooser = new DirectoryChooser();
        File dir = dirChooser.showDialog(app.getGUI().getWindow());
        String dirName = new File(dir.getPath()).getName();

        // Need to check if directories exist.
        if (!(new File(PATH_WORK + dirName)).exists()) {
            promptMessage(CODE_CHECK_LOADING_ERROR_TITLE.toString(), CODE_CHECK_LOADING_ERROR_MESSAGE.toString());
        } else {
            CodeCheckData dataManager = (CodeCheckData) app.getDataComponent();
            dataManager.resetData();
            dataManager.setCodeCheckName(dirName);

            app.getGUI().getWindow().setTitle(props.getProperty(TITLE_PREFIX) + dirName);

        }
        // Populate the tables
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        ListView step1ListView = workspace.getExtractSubListView();
        exportSubRefresh(step1ListView);
    }

    public void handleLoadRecent(String name) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        app.getGUI().getWindow().setTitle(props.getProperty(TITLE_PREFIX) + name);
        CodeCheckData dataManager = (CodeCheckData) app.getDataComponent();
        dataManager.setCodeCheckName(name);
        handleHomeView();
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        workspace.showTopToolbarPane();

        // Populate the tables
        ListView step1ListView = workspace.getExtractSubListView();
        ListView step2ListView = workspace.getRenameSubListView();
        ListView step3ListView = workspace.getUnzipSubListView();
        ListView step4ListView = workspace.getExtractSrcListView();
        ListView step5ListView = workspace.getCodeCheckListView();

        exportSubRefresh(step1ListView);
        refreshSubRefresh(step2ListView);
        refreshSubRefresh(step3ListView);
//        handleExportSubRefresh(step1ListView);
//        handleExportSubRefresh(step1ListView);

        if (step2ListView.getItems().size() > 0) {
            workspace.getRenameSubButton().setDisable(false);
        }
    }

    private boolean validName(String name) {
        Pattern pattern = Pattern.compile(ILLEGALCHARS);
        Matcher matcher = pattern.matcher(name);
        return matcher.find();

    }

    private void exportSubRefresh(ListView table) {
        CodeCheckData dataManager = (CodeCheckData) app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String pathToBlackboard = PATH_WORK + dataManager.getCodeCheckName() + props.getProperty(BLACKBOARD_PATH);
        ArrayList<String> contents = listAllFilesInDirectory(pathToBlackboard);
        table.getItems().clear();
        table.getItems().addAll(contents);
    }

    private void refreshSubRefresh(ListView table) {
        CodeCheckData dataManager = (CodeCheckData) app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String pathToBlackboard = PATH_WORK + dataManager.getCodeCheckName() + props.getProperty(SUBMISSIONS_PATH);
        ArrayList<String> contents = listAllFilesInDirectory(pathToBlackboard);
        table.getItems().clear();
        table.getItems().addAll(contents);
    }

    public void handleRefresh(ListView table, String directoryToCheck) {
        CodeCheckData dataManager = (CodeCheckData) app.getDataComponent();
        String pathToDirectory = PATH_WORK + dataManager.getCodeCheckName() + directoryToCheck;
        cleanDirectory(pathToDirectory, "txt");
        cleanDirectory(pathToDirectory, "rar");
        ArrayList<String> contents = listAllFilesInDirectory(pathToDirectory);

        table.getItems().clear();
        table.getItems().addAll(contents);
    }

    private ArrayList<String> listAllFilesInDirectory(String path) {

        ArrayList<String> contents = new ArrayList<>();

        File folder = new File(path);
        String[] files = folder.list();
        for (String file : files) {
            contents.add(file);
        }
        return contents;
    }

    public void handleZipView(ListView table, String directoryToCheck) {
        Object itemSelected = table.getSelectionModel().getSelectedItem();
        String item = itemSelected.toString();
        CodeCheckData dataManager = (CodeCheckData) app.getDataComponent();

        String pathToItem = PATH_WORK + dataManager.getCodeCheckName() + directoryToCheck
                + BACKSLASH + item;
        ArrayList<String> contents = listAllFilesInAZipFile(pathToItem);

        // Going to need to split this method to handle zip or directory
        String message = "";
        for (String str : contents) {
            message += str + "\n";
        }

        promptMessage2("Title", message);
    }

    public void handleDirectoryView(ListView table, String directoryToCheck) {
        Object itemSelected = table.getSelectionModel().getSelectedItem();
        String item = itemSelected.toString();
        CodeCheckData dataManager = (CodeCheckData) app.getDataComponent();

        String pathToItem = PATH_WORK + dataManager.getCodeCheckName() + directoryToCheck
                + BACKSLASH + item;
        ArrayList<String> contents = listAllFilesInDirectory(pathToItem);

        // Going to need to split this method to handle zip or directory
        String message = "";
        for (String str : contents) {
            message += str + "\n";
        }

        promptMessage2("Title", message);
    }

    private ArrayList<String> listAllFilesInAZipFile(String path) {
        ArrayList<String> dirContents = new ArrayList<>();

        try {
            ZipFile zipFile = new ZipFile(path);
            List fileHeaderList = zipFile.getFileHeaders();
            for (int i = 0; i < fileHeaderList.size(); i++) {
                FileHeader fileHeader = (FileHeader) fileHeaderList.get(i);
                String fileName = fileHeader.getFileName();
                dirContents.add(fileHeader.getFileName());
            }
        } catch (ZipException ex) {
            Logger.getLogger(CodeCheckController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dirContents;
    }

    private ArrayList<String> listAllZipFilesInZipFile(String path) {
        ArrayList<String> dirContents = new ArrayList<>();

        try {
            ZipFile zipFile = new ZipFile(path);

            List fileHeaderList = zipFile.getFileHeaders();
            for (int i = 0; i < fileHeaderList.size(); i++) {
                FileHeader fileHeader = (FileHeader) fileHeaderList.get(i);
                String fileName = fileHeader.getFileName();
                if (fileHeader.getFileName().contains(".zip")) {
                    File file = new File(path + BACKSLASH + fileHeader.getFileName());
                    String contents = fileHeader.getFileName();
                    if (!contents.contains(BACKSLASH)) {
                        System.out.println(path + BACKSLASH + fileHeader.getFileName());
                    }
                    dirContents.add(fileHeader.getFileName());
                }
            }
        } catch (ZipException ex) {
            Logger.getLogger(CodeCheckController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dirContents;
    }

    private ArrayList<String> listAllNonZipFilesInZip(String path) {
        ArrayList<String> dirContents = new ArrayList<>();

        try {
            ZipFile zipFile = new ZipFile(path);

            List fileHeaderList = zipFile.getFileHeaders();
            for (int i = 0; i < fileHeaderList.size(); i++) {
                FileHeader fileHeader = (FileHeader) fileHeaderList.get(i);
                if (!fileHeader.getFileName().contains(".zip")) {
                    File file = new File(path + BACKSLASH + fileHeader.getFileName());
                    String contents = fileHeader.getFileName();
                    if (!contents.contains(BACKSLASH)) {
                        System.out.println(path + BACKSLASH + fileHeader.getFileName());
                    }
                    dirContents.add(fileHeader.getFileName());
                }
            }
        } catch (ZipException ex) {
            Logger.getLogger(CodeCheckController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dirContents;
    }

    public void handleZipRemove(ListView table, String directoryToCheck) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Remove");
        alert.setHeaderText("You are about to remove this item.");
        alert.setContentText("Doing so will delete it permanetly, continue?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            Object itemSelected = table.getSelectionModel().getSelectedItem();
            String item = itemSelected.toString();
            CodeCheckData dataManager = (CodeCheckData) app.getDataComponent();
            String pathToItem = PATH_WORK + dataManager.getCodeCheckName()
                    + directoryToCheck + BACKSLASH + item;
            if (!removeFile(pathToItem)) {
                Alert errorAlert = new Alert(AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("");
                alert.setContentText("An error occured while removing!");
                alert.showAndWait();
            }
        }
        handleRefresh(table, directoryToCheck);
    }

    public void handleDirectoryRemove(ListView table, String directoryToCheck) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Remove");
        alert.setHeaderText("You are about to remove this item.");
        alert.setContentText("Doing so will delete it permanetly, continue?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            Object itemSelected = table.getSelectionModel().getSelectedItem();
            String item = itemSelected.toString();
            CodeCheckData dataManager = (CodeCheckData) app.getDataComponent();
            String pathToItem = PATH_WORK + dataManager.getCodeCheckName()
                    + directoryToCheck + BACKSLASH + item;
            deleteDirectory(new File(pathToItem));
        }
        handleRefresh(table, directoryToCheck);
    }

    // remember to give credit to stack overflow person who answered this Q
    public boolean deleteDirectory(File path) {
        if (path.exists()) {
            File[] files = path.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return (path.delete());
    }

    private boolean removeFile(String path) {
        return new File(path).delete();
    }

    public void handleBlackboardSubmissions(ListView table, String source, String destination) {
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        ObservableList<Object> selectedItems = table.getSelectionModel().getSelectedItems();
        CodeCheckData dataManager = (CodeCheckData) app.getDataComponent();
        ProgressBar extractSubmissionsPB = workspace.getExtractSubProgressBar();
        ProgressIndicator progressIndicator = workspace.getExtractProgressIndicator();
        TextArea textArea = workspace.getExtractSubTextArea();
        textArea.clear();
        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                try {

                    Platform.runLater(new Runnable() {
                        String successMessage = "Successfullly extracted files:\n";
                        String failureMessage = "Submission Errors:\n";

                        @Override
                        public void run() {
                            for (int i = 0; i < selectedItems.size(); i++) {
                                String item = selectedItems.get(i).toString();
                                String pathToItem = PATH_WORK + dataManager.getCodeCheckName() + source
                                        + BACKSLASH + item;
                                String extractionDestination = PATH_WORK + dataManager.getCodeCheckName() + destination;

                                try {
                                    if (!item.contains(ZIPEXTENSION)) {
                                        failureMessage += "-" + item;
                                    }
                                    ZipFile zipFile = new ZipFile(pathToItem);
                                    zipFile.extractAll(extractionDestination);

                                    ArrayList<String> contents = listAllZipFilesInZipFile(pathToItem);
                                    for (String str : contents) {
                                        successMessage += "-" + str + "\n";
                                    }

                                    contents = listAllNonZipFilesInZip(pathToItem);
                                    for (String str : contents) {
                                        failureMessage += "-" + str + "\n";
                                    }

                                    try {
                                        // SLEEP EACH FRAME
                                        Thread.sleep(10);
                                    } catch (InterruptedException ex) {
                                        Logger.getLogger(CodeCheckController.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                } catch (ZipException ex) {
                                    Logger.getLogger(CodeCheckController.class.getName()).log(Level.SEVERE, null, ex);
                                    //promptMessage2("title", "A corrupted file was detected: " + item);
                                }
                                updateProgress(i + 1, selectedItems.size());
                            }

                            textArea.appendText(successMessage + "\n");
                            textArea.appendText(failureMessage + "\n");
                            handleRefresh(workspace.getRenameSubListView(), PropertiesManager.getPropertiesManager().getProperty(SUBMISSIONS_PATH));

                        }

                    });

                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    workspace.getRenameSubButton().setDisable(false);

                }
                return null;
            }
        };
        Thread thread = new Thread(task);
        extractSubmissionsPB.progressProperty().bind(task.progressProperty());
        progressIndicator.progressProperty().bind(task.progressProperty());
        thread.start();
        cleanDirectory(PropertiesManager.getPropertiesManager().getProperty(SUBMISSIONS_PATH), "txt");
        cleanDirectory(PropertiesManager.getPropertiesManager().getProperty(SUBMISSIONS_PATH), "rar");

    }

    public void handleRename(ListView table, String directoryToCheck) {
        handleRefresh(table, directoryToCheck);
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        ObservableList<Object> selectedItems = table.getItems();
        CodeCheckData dataManager = (CodeCheckData) app.getDataComponent();
        ProgressBar renameSubmissionsPB = workspace.getRenameProgressBar();
        ProgressIndicator progressIndicator = workspace.getRenameProgressIndicator();

        TextArea textArea = workspace.getRenameSubTextArea();
        textArea.clear();
        workspace.getRenameButton().setDisable(true);
        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                try {
                    progressLock.lock();
                    workspace.getRenameButton().setDisable(true);
                    for (int i = 0; i < selectedItems.size(); i++) {
                        String item = selectedItems.get(i).toString();

                        Platform.runLater(new Runnable() {
                            String successMessage = "Successfullly extracted files:\n";
                            String failureMessage = "Submission Errors:\n";

                            @Override
                            public void run() {

                                String pathToItem = PATH_WORK + dataManager.getCodeCheckName() + directoryToCheck
                                        + BACKSLASH + item;
                                File fileToRename = new File(pathToItem);
                                String rename = getNetID(PATH_WORK + dataManager.getCodeCheckName() + directoryToCheck, pathToItem);

                                File fileRenamed = new File(rename);
                                if (rename.contains(item)) {
                                    textArea.appendText(item + " has already been renamed" + "\n");
                                } else if (fileRenamed.exists()) {
                                    fileToRename.delete();
                                    textArea.appendText("- " + fileToRename.getName() + " already existed " + "\n\n");
                                } else {
                                    textArea.appendText("- " + fileToRename.getName() + " becomes " + fileRenamed.getName() + "\n\n");
                                    fileToRename.renameTo(fileRenamed);
                                }

                                ArrayList<String> contents = listAllZipFilesInZipFile(pathToItem);
                                for (String str : contents) {
                                    successMessage += "-" + str + "\n";
                                }

                                contents = listAllNonZipFilesInZip(pathToItem);
                                for (String str : contents) {
                                    failureMessage += "-" + str + "\n";
                                }
                            }
                        });

                        updateProgress(i + 1, selectedItems.size());
                        // SLEEP EACH FRAME
                        Thread.sleep(10);
                    }
                } finally {
                    handleRefresh(table, directoryToCheck);
                    handleRefresh(workspace.getUnzipSubListView(), directoryToCheck);
                    workspace.getRenameButton().setDisable(false);
                    progressLock.unlock();
                }
                return null;
            }
        };

        Thread thread = new Thread(task);
        renameSubmissionsPB.progressProperty().bind(task.progressProperty());
        progressIndicator.progressProperty().bind(task.progressProperty());
        thread.start();

    }

    private String getNetID(String path, String oldName) {
        String delims = "_";
        String[] tokens = oldName.split(delims);

        if (tokens.length == 1) {
            return path + BACKSLASH + oldName + ZIPEXTENSION;
        }
        return path + BACKSLASH + tokens[1] + ZIPEXTENSION;
    }

    private String createStudentDirectoryName(String name) {
        String delims = ".zip";
        String[] tokens = name.split(delims);

        return tokens[0];
    }

    public void handleExtractStudentSubmission(ListView table, String sourcePath, String destinationPath) {
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        ObservableList<Object> selectedItems = table.getSelectionModel().getSelectedItems();
        CodeCheckData dataManager = (CodeCheckData) app.getDataComponent();
        ProgressBar progressBar = workspace.getUnzipProgressBar();
        ProgressIndicator progressIndicator = workspace.getUnzipProgressIndicator();
        TextArea textArea = workspace.getUnzipSubTextArea();
        textArea.clear();

        Task<Void> task = new Task<Void>() {
            String successful = "Successful code extraction:\n";
            @Override
            protected Void call() throws Exception {
                Platform.runLater(new Runnable() {
                    String successMessage = "Successfullly extracted files:\n";
                    String failureMessage = "Submission Errors:\n";

                    @Override
                    public void run() {
                        for (int i = 0; i < selectedItems.size(); i++) {

                            String item = selectedItems.get(i).toString();
                            String pathToItem = PATH_WORK + dataManager.getCodeCheckName() + sourcePath
                                    + BACKSLASH + item;
                            String destination = PATH_WORK + dataManager.getCodeCheckName() + destinationPath
                                    + BACKSLASH + item;
                            String studentDirName = createStudentDirectoryName(destination + BACKSLASH + item);
                            File destinationFolder = new File(studentDirName);
                            destinationFolder.mkdir();
                            successful += "-" + item + "\n";
                            
                            if (item.contains(".zip")) {
                                textArea.appendText("-" + item + " was unzipped successfully\n");
                            } else {
                                textArea.appendText("-" + item + " was unable to be unzipped\n");
                            }
                            try {
                                ZipFile zipFile = new ZipFile(pathToItem);
                                zipFile.extractAll(destinationFolder.getPath());
                                
                                try {
                                    Thread.sleep(10);
                                } catch (InterruptedException ex) {
                                    Logger.getLogger(CodeCheckController.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            } catch (ZipException ex) {
                                Logger.getLogger(CodeCheckController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            updateProgress(i + 1, selectedItems.size());
                        }
                        handleRefresh(workspace.getExtractSrcListView(), PropertiesManager.getPropertiesManager().getProperty(PROJECTS_PATH));
                    }
                });
                return null;
            }
        };
        Thread thread = new Thread(task);
        progressBar.progressProperty().bind(task.progressProperty());
        progressIndicator.progressProperty().bind(task.progressProperty());
        thread.start();
    }

    public void handleExtractSourceCode(ListView table, String source, String destination) {

        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        ObservableList<Object> selectedItems = table.getSelectionModel().getSelectedItems();
        CodeCheckData dataManager = (CodeCheckData) app.getDataComponent();
        ProgressBar progressBar = workspace.getExtractSourceCodeProgressBar();
        ProgressIndicator progressIndicator = workspace.getSourceCodeProgressIndicator();
        TextArea textArea = workspace.getExtractSourceTextArea();
        textArea.clear();
        boolean extractJava = workspace.extractJava();
        boolean extractJS = workspace.extractJS();
        boolean extractC = workspace.extractC();
        boolean extractCS = workspace.extractCS();
        boolean extractOther = workspace.extractOther();
        String otherExtension = workspace.getOtherExtension();

        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                for (int i = 0; i < selectedItems.size(); i++) {
                    String item = selectedItems.get(i).toString();
                    String pathToItem = PATH_WORK + dataManager.getCodeCheckName() + source
                            + BACKSLASH + item;
                    String pathToDestination = PATH_WORK + dataManager.getCodeCheckName() + destination
                            + BACKSLASH + item;

                    File sourceFolder = new File(pathToItem);
                    File destinationFolder = new File(pathToDestination);
                    destinationFolder.mkdir();

                    if (extractJava) {
                        extractCode(sourceFolder, destinationFolder, ".java");
                    }
                    if (extractJS) {
                        extractCode(sourceFolder, destinationFolder, ".js");
                    }
                    if (extractC) {
                        extractCode(sourceFolder, destinationFolder, ".c");
                        extractCode(sourceFolder, destinationFolder, ".h");
                        extractCode(sourceFolder, destinationFolder, ".cpp");
                    }
                    if (extractCS) {
                        extractCode(sourceFolder, destinationFolder, ".cs");
                    }
                    if (extractOther) {
                        extractCode(sourceFolder, destinationFolder, otherExtension);
                    }
                    updateProgress(i + 1, selectedItems.size());
                    // SLEEP EACH FRAME
                    Thread.sleep(10);
                }
                return null;
            }
        };
        Thread thread = new Thread(task);
        progressIndicator.progressProperty().bind(task.progressProperty());
        progressBar.progressProperty().bind(task.progressProperty());
        thread.start();
    }

    private void extractCode(File source, File destination, String fileExtension) {
        if (destination.exists()) {
            File[] files = source.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    extractCode(files[i], destination, fileExtension);
                } else {
                    try {
                        if (files[i].getName().contains(fileExtension)) {
                            System.out.println(files[i].getName() + " is a java file");
                            File sourceFile = new File(source + "\\" + files[i].getName());
                            FileUtils.copyFileToDirectory(sourceFile, destination);
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(CodeCheckController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    private String completePath(ListView table, String directoryToCheck) {
        Object itemSelected = table.getSelectionModel().getSelectedItem();
        String item = itemSelected.toString();
        CodeCheckData dataManager = (CodeCheckData) app.getDataComponent();

        // ./work/CodeCheckSelected/blackboard/pathToItemSelectedFromTable
        String pathToItem = PATH_WORK + dataManager.getCodeCheckName() + directoryToCheck
                + BACKSLASH + item;

        return pathToItem;
    }

    private void cleanDirectory(String directoryPathToClean, String extension) {

        File folder = new File(directoryPathToClean);
        String[] files = folder.list();
        for (String file : files) {
            if (file.contains(extension)) {
                removeFile(directoryPathToClean + BACKSLASH + file);
            }
        }
    }

    public void handleCodeCheck() {
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        ProgressBar progressBar = workspace.getCodeCheckProgressBar();
        ProgressIndicator progressIndicator = workspace.getCodeCheckProgressIndicator();
        TextArea textArea = workspace.getCodeCheckSubTextArea();
        textArea.clear();

        Task<Void> task = new Task<Void>() {
            int task = numTasks++;
            double max = 200;
            double perc;

            @Override
            protected Void call() throws Exception {
                try {
                    progressLock.lock();
                    for (int i = 0; i <= 200; i++) {
                        System.out.println(i);

                        perc = i / max;

                        // THIS WILL BE DONE ASYNCHRONOUSLY VIA MULTITHREADING
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                // WHAT'S MISSING HERE?
                                progressBar.setProgress(perc);
                                progressIndicator.setProgress(perc);
                            }
                        });

                        // SLEEP EACH FRAME
                        Thread.sleep(10);
                    }
                } finally {
                    // WHAT DO WE NEED TO DO HERE?
                    progressLock.unlock();
                }
                return null;
            }
        };
        // THIS GETS THE THREAD ROLLING
        Thread thread = new Thread(task);
        thread.start();
        textArea.appendText("Student Plageiarism Check results can be found at google.com");
    }

    public void handleViewResults() {
        // Create a WebView
        Stage browserStage = new Stage();
        Scene scene = new Scene(new Group());

        final WebView browser = new WebView();
        final WebEngine webEngine = browser.getEngine();

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(browser);

        webEngine.getLoadWorker().stateProperty()
                .addListener(new ChangeListener<Worker.State>() {
                    @Override
                    public void changed(ObservableValue ov, Worker.State oldState, Worker.State newState) {

                        if (newState == Worker.State.SUCCEEDED) {
                            browserStage.setTitle(webEngine.getLocation());
                        }

                    }
                });
        webEngine.load("http://google.com");
        scene.setRoot(scrollPane);
        browserStage.setScene(scene);
        browserStage.setResizable(false);
        browserStage.show();
    }

}
