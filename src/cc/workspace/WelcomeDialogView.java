/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.workspace;

import cc.CodeCheckApp;
import static cc.CodeCheckProp.TITLE_PREFIX;
import static cc.CodeCheckProp.WELCOME_DIALOG_BACKGROUND_IMAGE_PATH;
import static cc.style.CodeCheckStyle.CLASS_WELCOME_VIEW_HYPERLINK;
import static cc.style.CodeCheckStyle.CLASS_WELCOME_VIEW_IMAGE_VIEW;
import static cc.style.CodeCheckStyle.CLASS_WELCOME_VIEW_LABEL;
import static cc.style.CodeCheckStyle.CLASS_WELCOME_VIEW_MAIN_BOX;
import static cc.style.CodeCheckStyle.CLASS_WELCOME_VIEW_RECENT_WORK_BOX;
import static cc.style.CodeCheckStyle.CLASS_WELCOME_VIEW_TOOLBAR;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

/**
 *
 * @author aespi
 */
public class WelcomeDialogView extends BorderPane {
    
    CodeCheckApp app;
    CodeCheckController controller;
    
    private Image codeCheckImage;
    private ImageView codeCheckImageView;
    private SplitPane welcomeViewSplitPane;
    private VBox recentWorkVBox;
    private VBox mainSectionVBox;
    private Hyperlink newCodeCheckHyperlink;
    private Label recentWorkLabel;
    
    public WelcomeDialogView(CodeCheckApp initApp){
        app = initApp;
        initLayout();
        initControllers();
        initStyle();
    }
    
    private void initLayout(){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        newCodeCheckHyperlink = new Hyperlink("Create New Code Check");
        recentWorkLabel = new Label("Recent Work");
        
        codeCheckImage = new Image(FILE_PROTOCOL + PATH_IMAGES 
                + props.getProperty(WELCOME_DIALOG_BACKGROUND_IMAGE_PATH));
        codeCheckImageView = new ImageView(codeCheckImage);
        codeCheckImageView.setPreserveRatio(true);        
        codeCheckImageView.fitWidthProperty().bind(app.getGUI().getWindow().widthProperty().divide(1.3));
        codeCheckImageView.fitHeightProperty().bind(this.heightProperty().divide(2));
       
        
        recentWorkVBox = new VBox();
        recentWorkVBox.getChildren().add(recentWorkLabel);
        for(String recentWork : listFolders("./work/")){
            Hyperlink i = new Hyperlink(recentWork);
            i.getStyleClass().add(CLASS_WELCOME_VIEW_HYPERLINK);
            i.setPadding(new Insets(0,0,0,25));
            i.setOnAction(e ->{
                app.getGUI().getWindow().setTitle(props.getProperty(TITLE_PREFIX) + i.getText());
                controller.handleLoadRecent(i.getText());
            } );            
            recentWorkVBox.getChildren().add(i);            
        }
        // call method that populates with recent work
        mainSectionVBox = new VBox(350);
        mainSectionVBox.setAlignment(Pos.BASELINE_CENTER);
        
        mainSectionVBox.getChildren().add(codeCheckImageView);
        mainSectionVBox.getChildren().add(newCodeCheckHyperlink);
        
        welcomeViewSplitPane = new SplitPane();
        welcomeViewSplitPane.setDividerPositions(0.3);
        welcomeViewSplitPane.maxWidthProperty().multiply(0.2);
        SplitPane.setResizableWithParent(recentWorkVBox, Boolean.FALSE);
        SplitPane.setResizableWithParent(mainSectionVBox, Boolean.FALSE);
        SplitPane.setResizableWithParent(welcomeViewSplitPane, Boolean.FALSE);
        SplitPane.setResizableWithParent(codeCheckImageView, Boolean.FALSE);
        SplitPane.setResizableWithParent(this, Boolean.FALSE);
        welcomeViewSplitPane.setOrientation(Orientation.HORIZONTAL);
        welcomeViewSplitPane.getItems().add(recentWorkVBox);
        welcomeViewSplitPane.getItems().add(mainSectionVBox);
        this.setCenter(welcomeViewSplitPane);
        
        // NOTE TO REMOVE BORDER IN STYLE SET BOX BORDER TO TRANSPARANT
        // split.setStyle("-fx-box-border: transparent;");
        // or
        // -fx-padding: 0;
    }
            
    private ArrayList<String> listFolders(String directoryName){

        File directory = new File(directoryName);
        ArrayList<Long> lastModified = new ArrayList<>();
        ArrayList<String> recentWork = new ArrayList<>();
        //get all the files from a directory

        File[] fList = directory.listFiles();

        for (File file : fList){            
            if (file.isDirectory()){
                lastModified.add(file.lastModified());
            }
        }
        
        Collections.sort(lastModified);
        System.out.println(lastModified.toString());
        for(Long dateModified : lastModified){
            for(File file : fList){
                if(file.lastModified() == dateModified){
                    recentWork.add(file.getName());
                }
            }
        }
           
        Collections.reverse(recentWork);
        System.out.println(recentWork);

        return recentWork;
    }
    
    private void initControllers(){
        
        controller = new CodeCheckController(app);
        newCodeCheckHyperlink.setOnAction(e -> controller.handleNewCodeCheck());
    }

    private void initStyle() {
        recentWorkLabel.getStyleClass().add(CLASS_WELCOME_VIEW_LABEL); 
        app.getGUI().getTopToolbarPane().getStyleClass().add(CLASS_WELCOME_VIEW_TOOLBAR);
        recentWorkVBox.getStyleClass().add(CLASS_WELCOME_VIEW_RECENT_WORK_BOX);
        mainSectionVBox.getStyleClass().add(CLASS_WELCOME_VIEW_MAIN_BOX);
        mainSectionVBox.getStyleClass().add(CLASS_WELCOME_VIEW_IMAGE_VIEW);
        newCodeCheckHyperlink.getStyleClass().add(CLASS_WELCOME_VIEW_HYPERLINK);
    }
}
