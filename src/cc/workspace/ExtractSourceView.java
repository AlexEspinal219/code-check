/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.workspace;

import cc.CodeCheckApp;
import static cc.CodeCheckProp.CODE_CHECK_RESULTS_BUTTON_MESSAGE;
import static cc.CodeCheckProp.CODE_PATH;
import static cc.CodeCheckProp.EXTRACT_SOURCE_BUTTON_MESSAGE;
import static cc.CodeCheckProp.EXTRACT_SOURCE_CS_CHECKBOX_MESSAGE;
import static cc.CodeCheckProp.EXTRACT_SOURCE_C_CHECKBOX_MESSAGE;
import static cc.CodeCheckProp.EXTRACT_SOURCE_DIRECTIONS_MESSAGE;
import static cc.CodeCheckProp.EXTRACT_SOURCE_FILE_TYPE_LABEL_MESSAGE;
import static cc.CodeCheckProp.EXTRACT_SOURCE_JAVA_CHECKBOX_MESSAGE;
import static cc.CodeCheckProp.EXTRACT_SOURCE_JS_CHECKBOX_MESSAGE;
import static cc.CodeCheckProp.EXTRACT_SOURCE_LABEL_MESSAGE;
import static cc.CodeCheckProp.EXTRACT_SOURCE_LISTVIEW_TITLE;
import static cc.CodeCheckProp.EXTRACT_SOURCE_OTHER_CHECKBOX_MESSAGE;
import static cc.CodeCheckProp.EXTRACT_SOURCE_PROGRESS_MESSAGE;
import static cc.CodeCheckProp.PROJECTS_PATH;
import static cc.CodeCheckProp.WORKSPACE_REFRESH_BUTTON_MESSAGE;
import static cc.CodeCheckProp.WORKSPACE_REMOVE_BUTTON_MESSAGE;
import static cc.CodeCheckProp.WORKSPACE_VIEW_BUTTON_MESSAGE;
import cc.data.CodeCheck;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_DEFAULT_BUTTONS;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_LISTVIEW;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_LISTVIEW_LABEL;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_MAIN_LABEL;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_PROGRESS_BAR;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_SECONDARY_LABEL;
import static cc.style.CodeCheckStyle.CLASS_WORKSPACE_VIEW;
import javafx.beans.property.StringProperty;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

/**
 *
 * @author aespi
 */
public class ExtractSourceView extends BorderPane{
    
    
    CodeCheckApp app;
    CodeCheckController controller;
    
    private Label extractSubLabel;
    private Label extractDirectionsLabel;
    private Label progressLabel;
    private Label listViewLabel;
    private Label sourceFileTypesLabel;
    private ListView extractSubListView;
    private TextArea extractSubTextArea;
    private Button extractButton;
    private Button removeSubButton;
    private Button refreshSubButton;
    private Button viewSubButton;
    
    private ProgressBar extractProgressBar;
    private ProgressIndicator extractProgressIndicator;
    private GridPane mainViewGridPane;
    private HBox buttonsHBox;
    private HBox codeCheckHBox;
    private HBox progressHBox;
    
    private HBox otherOptionHBox;
    private GridPane checkBoxGridPane;
    private CheckBox javaCheckBox;
    private CheckBox jsCheckBox;
    private CheckBox cHCppCheckBox;
    private CheckBox csCheckBox;
    private CheckBox otherCheckBox;
    private TextField otherTextField;
    
    private String projectsSubmissionsPath;
    private String codeSubmissionsPath;
   
    
    public ExtractSourceView(CodeCheckApp initApp){
        app = initApp;
        initLayout();
        initControllers();
        initStyle();
        projectsSubmissionsPath = PropertiesManager.getPropertiesManager().getProperty(PROJECTS_PATH);
        codeSubmissionsPath = PropertiesManager.getPropertiesManager().getProperty(CODE_PATH);
    }
    
    private void initLayout(){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        extractSubLabel = new Label(props.getProperty(EXTRACT_SOURCE_LABEL_MESSAGE));
        extractDirectionsLabel = new Label(props.getProperty(EXTRACT_SOURCE_DIRECTIONS_MESSAGE));
        progressLabel = new Label(props.getProperty(EXTRACT_SOURCE_PROGRESS_MESSAGE));
        listViewLabel = new Label(props.getProperty(EXTRACT_SOURCE_LISTVIEW_TITLE));        
        extractButton = new Button(props.getProperty(EXTRACT_SOURCE_BUTTON_MESSAGE));
        removeSubButton = new Button(props.getProperty(WORKSPACE_REMOVE_BUTTON_MESSAGE));
        refreshSubButton = new Button(props.getProperty(WORKSPACE_REFRESH_BUTTON_MESSAGE));
        viewSubButton = new Button(props.getProperty(WORKSPACE_VIEW_BUTTON_MESSAGE));  
        extractButton.setDisable(true);
        removeSubButton.setDisable(true);
        viewSubButton.setDisable(true);
        extractSubListView = new ListView();
        extractSubListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        extractProgressBar = new ProgressBar();
        extractProgressBar.setProgress(0.00F);
        
        extractProgressIndicator = new ProgressIndicator();
        extractProgressIndicator.setProgress(0.00F);
        extractSubTextArea = new TextArea();
        extractSubTextArea.setEditable(false);
        extractSubTextArea.setWrapText(true);
        
        otherOptionHBox = new HBox(5);
        checkBoxGridPane = new GridPane();
        sourceFileTypesLabel = new Label(props.getProperty(EXTRACT_SOURCE_FILE_TYPE_LABEL_MESSAGE));
        javaCheckBox = new CheckBox(props.getProperty(EXTRACT_SOURCE_JAVA_CHECKBOX_MESSAGE));
        jsCheckBox = new CheckBox(props.getProperty(EXTRACT_SOURCE_JS_CHECKBOX_MESSAGE));
        cHCppCheckBox = new CheckBox(props.getProperty(EXTRACT_SOURCE_C_CHECKBOX_MESSAGE));
        csCheckBox = new CheckBox(props.getProperty(EXTRACT_SOURCE_CS_CHECKBOX_MESSAGE));
        otherCheckBox = new CheckBox();
        otherTextField = new TextField();
        otherTextField.setPromptText(props.getProperty(EXTRACT_SOURCE_OTHER_CHECKBOX_MESSAGE));
        otherOptionHBox.getChildren().add(otherCheckBox);
        otherOptionHBox.getChildren().add(otherTextField);
        checkBoxGridPane.add(sourceFileTypesLabel, 0, 0);
        checkBoxGridPane.add(javaCheckBox, 0, 1);
        checkBoxGridPane.add(cHCppCheckBox, 0, 2);
        checkBoxGridPane.add(otherOptionHBox, 0, 3);
        checkBoxGridPane.add(jsCheckBox, 1, 1);
        checkBoxGridPane.add(csCheckBox, 1, 2);
        
        
        buttonsHBox = new HBox(5);
        buttonsHBox.getChildren().add(removeSubButton);
        buttonsHBox.getChildren().add(refreshSubButton);
        buttonsHBox.getChildren().add(viewSubButton);
        
        progressHBox = new HBox(5);
        progressHBox.getChildren().add(progressLabel);
        progressHBox.getChildren().add(extractProgressBar);
        progressHBox.getChildren().add(extractProgressIndicator);
        
        codeCheckHBox = new HBox(5);
        codeCheckHBox.getChildren().add(extractButton);
        
        GridPane.setHgrow(this, Priority.ALWAYS);
        GridPane.setVgrow(this, Priority.ALWAYS);
        mainViewGridPane = new GridPane();
        mainViewGridPane.setVgap(10);
        mainViewGridPane.setHgap(50);
        //mainViewGridPane.setGridLinesVisible(true);
        mainViewGridPane.prefWidthProperty().bind(app.getGUI().getWindow().widthProperty().divide(1));
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(50);
        mainViewGridPane.getColumnConstraints().add(column1);
        
        mainViewGridPane.add(extractSubLabel, 0, 0);
        mainViewGridPane.add(extractDirectionsLabel, 0, 1);
        mainViewGridPane.add(listViewLabel, 0, 2);
        mainViewGridPane.add(extractSubListView, 0, 3);
        mainViewGridPane.add(buttonsHBox, 0, 4);
        mainViewGridPane.add(checkBoxGridPane, 0, 5);
        mainViewGridPane.add(progressHBox, 1, 0);
        mainViewGridPane.add(codeCheckHBox, 1, 1);
        mainViewGridPane.add(extractSubTextArea, 1, 3);        
        
        this.setCenter(mainViewGridPane);
    }
        
    private void initControllers(){
        // *** POSSIBLE SOURCE OF FUTURE BUG
        // DOES HAVING MULTIPLE INSTANCES OF THE CONTROLLER CAUSE ISSUES? YES
        
        controller = new CodeCheckController(app);
        
        removeSubButton.setOnAction(e -> controller.handleDirectoryRemove(extractSubListView, projectsSubmissionsPath));
        refreshSubButton.setOnAction(e -> controller.handleRefresh(extractSubListView, projectsSubmissionsPath));
        viewSubButton.setOnAction(e -> controller.handleDirectoryView(extractSubListView, projectsSubmissionsPath));
        extractButton.setOnAction(e -> controller.handleExtractSourceCode(extractSubListView, projectsSubmissionsPath, codeSubmissionsPath));
        
        extractSubListView.setOnMouseClicked(e ->{
            Object itemSelected = extractSubListView.getSelectionModel().getSelectedItem();
            if(itemSelected != null){
                viewSubButton.setDisable(false);
                removeSubButton.setDisable(false);
                extractButton.setDisable(false);
                
            }
        });
        this.setOnMouseClicked(e -> {
            extractSubListView.getSelectionModel().clearSelection();
            viewSubButton.setDisable(true);
            removeSubButton.setDisable(true);
            extractButton.setDisable(true);
        });
        extractSubTextArea.setOnMouseClicked(e -> {
            extractSubListView.getSelectionModel().clearSelection();
            viewSubButton.setDisable(true);
            removeSubButton.setDisable(true);
            extractButton.setDisable(true);
        });
        
        javaCheckBox.setOnAction(e ->{});
    }

    private void initStyle() {
        this.getStyleClass().add(CLASS_WORKSPACE_VIEW);
        extractSubLabel.getStyleClass().add(CLASS_WORKSPACE_MAIN_LABEL);
        extractDirectionsLabel.getStyleClass().add(CLASS_WORKSPACE_SECONDARY_LABEL);
        listViewLabel.getStyleClass().add(CLASS_WORKSPACE_LISTVIEW_LABEL);
        extractSubListView.getStyleClass().add(CLASS_WORKSPACE_LISTVIEW);
        progressLabel.getStyleClass().add(CLASS_WORKSPACE_MAIN_LABEL);
        removeSubButton.getStyleClass().add(CLASS_WORKSPACE_DEFAULT_BUTTONS);
        refreshSubButton.getStyleClass().add(CLASS_WORKSPACE_DEFAULT_BUTTONS);
        viewSubButton.getStyleClass().add(CLASS_WORKSPACE_DEFAULT_BUTTONS);
        extractButton.getStyleClass().add(CLASS_WORKSPACE_DEFAULT_BUTTONS);
        
        extractProgressBar.getStyleClass().add(CLASS_WORKSPACE_PROGRESS_BAR);
        mainViewGridPane.setPadding(new Insets(25,0,0,20));
        removeSubButton.setPadding(new Insets(5,35,5,35));
        refreshSubButton.setPadding(new Insets(5,35,5,35));
        viewSubButton.setPadding(new Insets(5,35,5,35));
        extractButton.setPadding(new Insets(5,35,5,35));
        
    }
    
    public boolean extractJava(){
        return javaCheckBox.isSelected();
    }
    
    public boolean extractJS(){
        return jsCheckBox.isSelected();
    }
    
    public boolean extractC(){
        return cHCppCheckBox.isSelected();
    }
    
    public boolean extractCS(){
        return csCheckBox.isSelected();
    }
    
    public boolean extractOther(){
        return otherCheckBox.isSelected();
    }

    public ListView getExtractSrcListView() {
        return extractSubListView;
    }    

    public TextArea getExtractSourceTextArea() {
        return extractSubTextArea;
    }

    public ProgressBar getExtractSourceCodeProgressBar() {
        return extractProgressBar;
    }

    public ProgressIndicator getExtractSourceCodeProgressIndicator() {
        return extractProgressIndicator;
    }
    
    public String getOtherExtension() {
        return otherTextField.getText();
    }
    
}
