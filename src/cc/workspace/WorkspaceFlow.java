/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.workspace;

import cc.CodeCheckApp;
import java.util.LinkedList;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;

/**
 *
 * @author aespi
 */
public class WorkspaceFlow extends LinkedList<BorderPane> {
    
    // To grant us access to the application
    CodeCheckApp app;
        
    // The different views that the user will use.
    private WelcomeDialogView welcomeDialogView;
    private ExtractSubmissionView extactSubmissionView;
    private RenameSubmissionsView renameSubmissionsView;
    private UnzipSubmissionView unzipSubmissionView;
    private ExtractSourceView extractSourceView;
    private CodeCheckView codeCheckView;
    public WorkspaceFlow(CodeCheckApp initApp){
        app = initApp;
        welcomeDialogView = new WelcomeDialogView(app);
        extactSubmissionView = new ExtractSubmissionView(app);
        renameSubmissionsView = new RenameSubmissionsView(app);
        unzipSubmissionView = new UnzipSubmissionView(app);
        extractSourceView = new ExtractSourceView(app);
        codeCheckView = new CodeCheckView(app);
        
        this.add(welcomeDialogView);
        this.add(extactSubmissionView);
        this.add(renameSubmissionsView);
        this.add(unzipSubmissionView);
        this.add(extractSourceView);
        this.add(codeCheckView);
    }
    
    public BorderPane getHomeView(){
        return extactSubmissionView;
    }
    
    public BorderPane getView(int view){
        BorderPane currentBorderPane = this.get(view);
        return currentBorderPane;
    }

    public BorderPane getWelcomeView() {
        return welcomeDialogView;
    }

    void resetViews() {
        welcomeDialogView = new WelcomeDialogView(app);
        extactSubmissionView = new ExtractSubmissionView(app);
        renameSubmissionsView = new RenameSubmissionsView(app);
        unzipSubmissionView = new UnzipSubmissionView(app);
        extractSourceView = new ExtractSourceView(app);
        codeCheckView = new CodeCheckView(app);
    }
    
    // Accessors to controls from the individual views
    
    // EXTRACT SUBMISSIONS VIEW - STEP 1
    public ListView getExtractSubListView(){
        return extactSubmissionView.getExtractSubListView();
    }
        
    public ProgressBar getExtractSubProgressBar(){
        return extactSubmissionView.getBlackboardSubmissionProgressBar();
    }
    
    public ProgressIndicator getExtractProgressIndicator(){
        return extactSubmissionView.getBlackboardSubmissionProgressIndicator();
    }
    
    public TextArea getExtractSubTextArea() {
        return extactSubmissionView.getExtractSubTextArea();
    }
    
    // RENAME VIEW - STEP 2
    public ListView getRenameSubListView(){
        return renameSubmissionsView.getRenameSubListView();
    }
    
    public TextArea getRenameSubTextArea() {
        return renameSubmissionsView.getRenameSubTextArea();
    }

    public ProgressBar getRenameProgressBar() {
        return renameSubmissionsView.getRenameProgressBar();
    }

    public ProgressIndicator getRenameProgressIndicator() {
        return renameSubmissionsView.getRenameProgressIndicator();
    }

    public Button getRenameButton() {
        return renameSubmissionsView.getRenameButton();
    }
    
    
    // UNZIP SUBMISSION VIEW - STEP 3
    public ListView getUnzipSubListView(){
        return unzipSubmissionView.getUnzipSubListView();
    }    
    
    public TextArea getUnzipSubTextArea() {
        return unzipSubmissionView.getUnzipSubTextArea();
    }

    public ProgressBar getUnzipProgressBar() {
        return unzipSubmissionView.getUnzipProgressBar();
    }

    public ProgressIndicator getUnzipProgressIndicator() {
        return unzipSubmissionView.getUnzipProgressIndicator();
    }
    
    // EXTRACT SOURCE CODE VIEW - STEP 4
    public ListView getExtractSrcListView(){
        return extractSourceView.getExtractSrcListView();
    }

    public ProgressBar getExtractSourceCodeProgressBar() {
        return extractSourceView.getExtractSourceCodeProgressBar();
    }

    public ProgressIndicator getSourceCodeProgressIndicator() {
        return extractSourceView.getExtractSourceCodeProgressIndicator();
    }
    
    public boolean extractJava(){
        return extractSourceView.extractJava();
    }
    
    public boolean extractJS(){
        return extractSourceView.extractJS();
    }
    
    public boolean extractC(){
        return extractSourceView.extractC();
    }
    
    public boolean extractCS(){
        return extractSourceView.extractCS();
    }
    
    public boolean extractOther(){
        return extractSourceView.extractOther();
    }
    
    public String getOtherExtension() {
        return extractSourceView.getOtherExtension();
    }
    
    public TextArea getExtractSourceTextArea(){
        return extractSourceView.getExtractSourceTextArea();
    }
        
    // CODE CHECK VIEW - STEP 5
    public ListView getCodeCheckListView(){
        return codeCheckView.getCodeCheckSubListView();
    }
    
    public TextArea getCodeCheckSubTextArea() {
        return codeCheckView.getCodeCheckSubTextArea();
    }
    
    public ProgressBar getCodeCheckProgressBar() {
        return codeCheckView.getCodeCheckProgressBar();
    }

    public ProgressIndicator getCodeCheckProgressIndicator() {
        return codeCheckView.getCodeCheckProgressIndicator();
    }
    
    
    
    
    
    
}
