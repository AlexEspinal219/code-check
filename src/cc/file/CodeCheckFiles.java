/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.file;

import cc.CodeCheckApp;
import cc.data.CodeCheck;
import cc.data.CodeCheckData;
import cc.workspace.CodeCheckController;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import static djf.settings.AppStartupConstants.PATH_WORK;
import djf.ui.AppMessageDialogSingleton;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.stage.DirectoryChooser;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import properties_manager.PropertiesManager;

/**
 *
 * @author aespi
 */
public class CodeCheckFiles implements AppFileComponent {

    CodeCheckApp app;
    
    public CodeCheckFiles(CodeCheckApp initApp) {
        app = initApp;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
//        // GET THE DATA
//	CodeCheckData dataManager = (CodeCheckData)data;
//        PropertiesManager props = PropertiesManager.getPropertiesManager();
//              
//        
//	// NOW BUILD THE SLIDES JSON OBJECTS TO SAVE
//        JsonObject codeCheckTitle = Json.createObjectBuilder()
//                .add("", dataManager.getCodeCheckName()).build();
//	JsonArrayBuilder slidesArrayBuilder = Json.createArrayBuilder();
//	        
//	// THEN PUT IT ALL TOGETHER IN A JsonObject
//	JsonObject dataManagerJSO = Json.createObjectBuilder()
//		.add("Address",codeCheckTitle)
//		.build();
//	
//	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
//	Map<String, Object> properties = new HashMap<>(1);
//	properties.put(JsonGenerator.PRETTY_PRINTING, true);
//	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
//	StringWriter sw = new StringWriter();
//	JsonWriter jsonWriter = writerFactory.createWriter(sw);
//	jsonWriter.writeObject(dataManagerJSO);
//	jsonWriter.close();
//
//	// INIT THE WRITER
//	OutputStream os = new FileOutputStream(filePath);
//	JsonWriter jsonFileWriter = Json.createWriter(os);
//	jsonFileWriter.writeObject(dataManagerJSO);
//	String prettyPrinted = sw.toString();
//	PrintWriter pw = new PrintWriter(filePath);
//	pw.write(prettyPrinted);
//	pw.close();
    }

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        DirectoryChooser dirChooser = new DirectoryChooser();  
        File dir = dirChooser.showDialog(app.getGUI().getWindow());
        String dirName = new File(dir.getPath()).getName();
        System.out.print(dirName);
        
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
