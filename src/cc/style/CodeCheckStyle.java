/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.style;

/**
 *
 * @author aespi
 */
public class CodeCheckStyle {
    
    public static String CLASS_NAVIGATION_PANE = "navigation_pane";
    public static String CLASS_NAVIGATION_BUTTON = "navigation_button";
    public static String CLASS_WELCOME_VIEW_LABEL = "welcome_label";
    public static String CLASS_WELCOME_VIEW_TOOLBAR = "welcome_toolbar";
    public static String CLASS_WELCOME_VIEW_RECENT_WORK_BOX = "welcome_recent_box";
    public static String CLASS_WELCOME_VIEW_MAIN_BOX = "welcome_main_box";
    public static String CLASS_WELCOME_VIEW_IMAGE_VIEW = "welcome_image_view";
    public static String CLASS_WELCOME_VIEW_HYPERLINK = "welcome_hyperlink";
    public static String CLASS_WORKSPACE_VIEW = "workspace_view";
    public static String CLASS_WORKSPACE_MAIN_LABEL = "main_workspace_label";
    public static String CLASS_WORKSPACE_SECONDARY_LABEL = "secondary_workspace_label";
    public static String CLASS_WORKSPACE_LISTVIEW_LABEL = "listview_workspace_label";
    public static String CLASS_WORKSPACE_LISTVIEW = "listview_workspace";
    public static String CLASS_WORKSPACE_GRIDPANE = "gridpane_workspace";
    public static String CLASS_WORKSPACE_DEFAULT_BUTTONS = "default_buttons_workspace";
    public static String CLASS_WORKSPACE_PROGRESS_BAR = "progress_bar_workspace";
    
}
